#![cfg_attr(not(feature = "std"), no_std)]

#[macro_use]
extern crate alloc;

mod error;
#[macro_use]
mod macros;
#[cfg(feature = "std")]
pub mod svd;
pub mod tree;

pub use error::*;
