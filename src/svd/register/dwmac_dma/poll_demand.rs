use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the Synopsys DesignWare Gigabit Ethernet MAC DMA Transmit/Receive Poll Demand register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "poll_demand",
        "Poll Demand - poll_demand0: Transmit, poll_demand1: Receive",
        // Offset in cluster, absolute offset: 0x1004-0x1008
        0x4,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "poll_demand",
            "Poll Demand",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            None,
        )?]),
        Some(
            svd::DimElement::builder()
                .dim(2)
                .dim_increment(0x4)
                .dim_index(Some([String::from("_tx"), String::from("_rx")].into()))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
