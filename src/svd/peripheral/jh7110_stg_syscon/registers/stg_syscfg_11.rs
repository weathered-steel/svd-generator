use super::create_register_stg_syscfg;
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 11 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register_stg_syscfg(
        44,
        "u0_hifi4_altresetvec",
        "Reset Vector Address",
        "[31:0]",
        svd::Access::ReadWrite,
        None,
    )
}
