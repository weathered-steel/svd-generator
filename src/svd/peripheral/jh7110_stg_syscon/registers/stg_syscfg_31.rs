use crate::svd::register::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 31 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "stg_syscfg_31",
        "STG SYSCONSAIF SYSCFG 124",
        0x7c,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "u0_pcie_axi4_mst0_awuser_42_32",
                "PCIE AXI4 MST0 AWUSER",
                create_bit_range("[10:0]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field_constraint(
                "u0_pcie_axi4_mst0_rderr",
                "PCIE AXI4 MST0 RDERR",
                create_bit_range("[18:11]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xff)?,
                None,
            )?,
        ]),
        None,
    )?))
}
