use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 88 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "stg_syscfg_88",
        "STG SYSCONSAIF SYSCFG 352",
        0x160,
        create_register_properties(32, 0x1)?,
        Some(&[
            create_field(
                "u0_pcie_pl_wake_in",
                "PCIE PL Wake IN",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "u0_pcie_pl_wake_oen",
                "PCIE PL Wake OEN",
                create_bit_range("[1:1]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "u0_pcie_rx_standby_0",
                "PCIE RX Standby",
                create_bit_range("[2:2]")?,
                svd::Access::ReadOnly,
                None,
            )?,
        ]),
        None,
    )?))
}
