use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties, jh7110,
};
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 68 register.
pub fn create() -> Result<svd::RegisterCluster> {
    let [slp, sd, rtsel, ptsel, trb, wtsel, vs, vg] =
        jh7110::create_fields_sram_config("u0_hifi4_scfg_sram_config", 0)?;

    Ok(svd::RegisterCluster::Register(create_register(
        "stg_syscfg_17",
        "STG SYSCONSAIF SYSCFG 68",
        0x44,
        create_register_properties(32, 0x0d54)?,
        Some(&[
            slp,
            sd,
            rtsel,
            ptsel,
            trb,
            wtsel,
            vs,
            vg,
            create_field(
                "u0_hifi4_statvectorsel",
                "When the value is 1, it indicates that the AltResetVec is valid",
                create_bit_range("[12:12]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "u0_hifi4_trigin_idma",
                "DMA port trigger",
                create_bit_range("[13:13]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "u0_hifi4_trigout_idma",
                "DMA port trigger",
                create_bit_range("[14:14]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "u0_hifi4_xocdmode",
                "Debug signal",
                create_bit_range("[15:15]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "u0_pcie_align_detect",
                "",
                create_bit_range("[16:16]")?,
                svd::Access::ReadOnly,
                None,
            )?,
        ]),
        None,
    )?))
}
