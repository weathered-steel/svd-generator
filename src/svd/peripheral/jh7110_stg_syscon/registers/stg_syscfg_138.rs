use super::create_register_stg_syscfg;
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 137 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register_stg_syscfg(
        552,
        "u0_pcie_axi4_mst0_awuser_31_0",
        "PCIE AXI4 AWUSER (little-endian)",
        "[31:0]",
        svd::Access::ReadOnly,
        None,
    )
}
