use crate::svd::register::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 233 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "stg_syscfg_233",
        "STG SYSCONSAIF SYSCFG 932",
        0x3a4,
        create_register_properties(32, 0x8)?,
        Some(&[
            create_field_constraint(
                "u1_pcie_tx_pattern",
                "PCIE TX Pattern",
                create_bit_range("[1:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field_constraint(
                "u1_pcie_usb3_bus_width",
                "PCIE USB3 Bus Width",
                create_bit_range("[3:2]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field(
                "u1_pcie_usb3_phy_enable",
                "PCIE USB3 PHY Enable",
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "u1_pcie_usb3_rate",
                "PCIE USB3 Rate",
                create_bit_range("[6:5]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field(
                "u1_pcie_usb3_rx_standby",
                "PCIE USB3 RX Standby",
                create_bit_range("[7:7]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "u1_pcie_xwdecerr",
                "PCIE XWDECERR",
                create_bit_range("[8:8]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "u1_pcie_xwerrclr",
                "PCIE XWERRCLR",
                create_bit_range("[9:9]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "u1_pcie_xwslverr",
                "PCIE XWSLVERR",
                create_bit_range("[10:10]")?,
                svd::Access::ReadOnly,
                None,
            )?,
        ]),
        None,
    )?))
}
