use super::create_register_stg_syscfg;
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 135 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register_stg_syscfg(
        540,
        "u1_pcie_axi4_mst0_aruser_31_0",
        "PCIE AXI4 ARUSER (little-endian)",
        "[31:0]",
        svd::Access::ReadOnly,
        None,
    )
}
