use super::create_register_stg_syscfg;
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 77 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register_stg_syscfg(
        308,
        "u0_pcie_local_interrupt_in",
        "PCIE Local Interrupt IN",
        "[31:0]",
        svd::Access::ReadWrite,
        None,
    )
}
