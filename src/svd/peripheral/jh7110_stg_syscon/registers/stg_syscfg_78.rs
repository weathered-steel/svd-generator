use crate::svd::register::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 78 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "stg_syscfg_78",
        "STG SYSCONSAIF SYSCFG 312",
        0x138,
        create_register_properties(32, 0x480_0001)?,
        Some(&[
            create_field(
                "u0_pcie_mperstn",
                "PCIE MPERSTN",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "u0_pcie_ebuf_mode",
                "PCIE EBUF Mode",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "u0_pcie_phy_test_cfg",
                "PCIE PHY Test Config",
                create_bit_range("[24:2]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7f_ffff)?,
                None,
            )?,
            create_field(
                "u0_pcie_rx_eq_training",
                "PCIE RX EQ Training",
                create_bit_range("[25:25]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "u0_pcie_rxterm_en",
                "PCIE RXTERM Enable",
                create_bit_range("[26:26]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "u0_pcie_tx_onezeros",
                "PCIE TX One Zeros",
                create_bit_range("[27:27]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
