use super::create_register_stg_syscfg;
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 185 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register_stg_syscfg(
        740,
        "u1_pcie_local_interrupt_in",
        "PCIE Local Interrupt IN",
        "[31:0]",
        svd::Access::ReadWrite,
        None,
    )
}
