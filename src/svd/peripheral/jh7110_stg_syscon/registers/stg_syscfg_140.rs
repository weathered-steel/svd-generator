use super::create_register_stg_syscfg;
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 140 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register_stg_syscfg(
        560,
        "u1_pcie_axi4_mst0_ruser",
        "PCIE AXI4 RUSER",
        "[31:0]",
        svd::Access::ReadWrite,
        None,
    )
}
