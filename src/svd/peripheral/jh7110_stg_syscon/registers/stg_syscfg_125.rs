use crate::svd::register::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint, jh7110,
};
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 125 register.
pub fn create() -> Result<svd::RegisterCluster> {
    let [slp, sd, rtsel, ptsel, trb, wtsel, vs, vg] =
        jh7110::create_fields_sram_config("u0_sec_top_sramcfg", 11)?;

    Ok(svd::RegisterCluster::Register(create_register(
        "stg_syscfg_125",
        "STG SYSCONSAIF SYSCFG 500",
        0x1f4,
        create_register_properties(32, 0x6a_a008)?,
        Some(&[
            create_field_constraint(
                "u0_pcie_tx_pattern",
                "PCIE TX Pattern",
                create_bit_range("[1:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field_constraint(
                "u0_pcie_usb3_bus_width",
                "PCIE USB3 Bus Width",
                create_bit_range("[3:2]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field(
                "u0_pcie_usb3_phy_enable",
                "PCIE USB3 PHY Enable",
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "u0_pcie_usb3_rate",
                "PCIE USB3 Rate",
                create_bit_range("[6:5]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field(
                "u0_pcie_usb3_rx_standby",
                "PCIE USB3 RX Standby",
                create_bit_range("[7:7]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "u0_pcie_xwdecerr",
                "PCIE XWDECERR",
                create_bit_range("[8:8]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "u0_pcie_xwerrclr",
                "PCIE XWERRCLR",
                create_bit_range("[9:9]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "u0_pcie_xwslverr",
                "PCIE XWSLVERR",
                create_bit_range("[10:10]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            slp,
            sd,
            rtsel,
            ptsel,
            trb,
            wtsel,
            vs,
            vg,
            create_field(
                "u0_pcie_align_detect",
                "PCIE Align Detect",
                create_bit_range("[23:23]")?,
                svd::Access::ReadOnly,
                None,
            )?,
        ]),
        None,
    )?))
}
