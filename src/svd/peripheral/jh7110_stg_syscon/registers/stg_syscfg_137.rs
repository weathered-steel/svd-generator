use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 137 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "stg_syscfg_137",
        "STG SYSCONSAIF SYSCFG 548",
        0x224,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "u1_pcie_axi4_mst0_awfunc",
                "",
                create_bit_range("[14:0]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "u1_pcie_axi4_mst0_awregion",
                "",
                create_bit_range("[18:15]")?,
                svd::Access::ReadOnly,
                None,
            )?,
        ]),
        None,
    )?))
}
