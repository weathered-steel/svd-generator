use super::create_register_stg_syscfg;
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 28 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register_stg_syscfg(
        112,
        "u0_pcie_axi4_mst0_aruser_52_32",
        "PCIE AXI4 ARUSER (little-endian)",
        "[20:0]",
        svd::Access::ReadOnly,
        None,
    )
}
