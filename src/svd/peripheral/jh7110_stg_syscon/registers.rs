use crate::svd::register::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

pub mod stg_syscfg_0;
pub mod stg_syscfg_1;
pub mod stg_syscfg_10;
pub mod stg_syscfg_11;
pub mod stg_syscfg_12;
pub mod stg_syscfg_123;
pub mod stg_syscfg_124;
pub mod stg_syscfg_125;
pub mod stg_syscfg_13;
pub mod stg_syscfg_134;
pub mod stg_syscfg_135;
pub mod stg_syscfg_136;
pub mod stg_syscfg_137;
pub mod stg_syscfg_138;
pub mod stg_syscfg_139;
pub mod stg_syscfg_14;
pub mod stg_syscfg_140;
pub mod stg_syscfg_141;
pub mod stg_syscfg_15;
pub mod stg_syscfg_150;
pub mod stg_syscfg_151;
pub mod stg_syscfg_152;
pub mod stg_syscfg_153;
pub mod stg_syscfg_154;
pub mod stg_syscfg_155;
pub mod stg_syscfg_156;
pub mod stg_syscfg_157;
pub mod stg_syscfg_16;
pub mod stg_syscfg_17;
pub mod stg_syscfg_184;
pub mod stg_syscfg_185;
pub mod stg_syscfg_186;
pub mod stg_syscfg_190;
pub mod stg_syscfg_191;
pub mod stg_syscfg_196;
pub mod stg_syscfg_2;
pub mod stg_syscfg_231;
pub mod stg_syscfg_232;
pub mod stg_syscfg_233;
pub mod stg_syscfg_26;
pub mod stg_syscfg_27;
pub mod stg_syscfg_28;
pub mod stg_syscfg_29;
pub mod stg_syscfg_3;
pub mod stg_syscfg_30;
pub mod stg_syscfg_31;
pub mod stg_syscfg_32;
pub mod stg_syscfg_33;
pub mod stg_syscfg_4;
pub mod stg_syscfg_42;
pub mod stg_syscfg_43;
pub mod stg_syscfg_44;
pub mod stg_syscfg_45;
pub mod stg_syscfg_46;
pub mod stg_syscfg_47;
pub mod stg_syscfg_48;
pub mod stg_syscfg_49;
pub mod stg_syscfg_5;
pub mod stg_syscfg_6;
pub mod stg_syscfg_7;
pub mod stg_syscfg_76;
pub mod stg_syscfg_77;
pub mod stg_syscfg_78;
pub mod stg_syscfg_8;
pub mod stg_syscfg_82;
pub mod stg_syscfg_83;
pub mod stg_syscfg_88;
pub mod stg_syscfg_9;

/// Creates StarFive JH7110 STG Syscon (compatible) register definitions.
pub fn create() -> Result<Vec<svd::RegisterCluster>> {
    let mut regs = Vec::with_capacity(233);

    regs.extend_from_slice(&[
        stg_syscfg_0::create()?,
        stg_syscfg_1::create()?,
        stg_syscfg_2::create()?,
        stg_syscfg_3::create()?,
        stg_syscfg_4::create()?,
        stg_syscfg_5::create()?,
        stg_syscfg_6::create()?,
        stg_syscfg_7::create()?,
        stg_syscfg_8::create()?,
        stg_syscfg_9::create()?,
        stg_syscfg_10::create()?,
        stg_syscfg_11::create()?,
        stg_syscfg_12::create()?,
        stg_syscfg_13::create()?,
        stg_syscfg_14::create()?,
        stg_syscfg_15::create()?,
        stg_syscfg_16::create()?,
        stg_syscfg_17::create()?,
    ]);

    for idx in 0..8 {
        let bit_off = idx * 32;
        let bit_off_end = bit_off + 31;

        regs.push(create_register_stg_syscfg(
            72 + (idx * 4),
            format!("u0_pcie_axi4_mst0_aratomop_{bit_off_end}_{bit_off}").as_str(),
            "PCIE AXI4 ARATOMOP MST0 (little-endian)",
            "[31:0]",
            svd::Access::ReadOnly,
            None,
        )?);
    }

    regs.extend_from_slice(&[
        stg_syscfg_26::create()?,
        stg_syscfg_27::create()?,
        stg_syscfg_28::create()?,
        stg_syscfg_29::create()?,
        stg_syscfg_30::create()?,
        stg_syscfg_31::create()?,
        stg_syscfg_32::create()?,
        stg_syscfg_33::create()?,
    ]);

    for idx in 0..8 {
        let bit_off = idx * 32;
        let bit_off_end = bit_off + 31;

        regs.push(create_register_stg_syscfg(
            136 + (idx * 4),
            format!("u0_pcie_axi4_slv0_aratomop_{bit_off_end}_{bit_off}").as_str(),
            "PCIE AXI4 ARATOMOP SLV0 (little-endian)",
            "[31:0]",
            svd::Access::ReadWrite,
            None,
        )?);
    }

    regs.extend_from_slice(&[
        stg_syscfg_42::create()?,
        stg_syscfg_43::create()?,
        stg_syscfg_44::create()?,
        stg_syscfg_45::create()?,
        stg_syscfg_46::create()?,
        stg_syscfg_47::create()?,
        stg_syscfg_48::create()?,
        stg_syscfg_49::create()?,
    ]);

    for idx in 0..26 {
        let bit_off = idx * 32;
        let bit_off_end = bit_off + 31;

        regs.push(create_register_stg_syscfg(
            200 + (idx * 4),
            format!("u0_pcie_k_phyparam_{bit_off_end}_{bit_off}").as_str(),
            "PCIE PHY Parameter (little-endian)",
            "[31:0]",
            svd::Access::ReadWrite,
            None,
        )?);
    }

    regs.extend_from_slice(&[
        stg_syscfg_76::create()?,
        stg_syscfg_77::create()?,
        stg_syscfg_78::create()?,
    ]);

    for idx in 0..3 {
        regs.push(create_register_stg_syscfg(
            316 + (idx * 4),
            format!("u0_pcie_pf{idx}_offset").as_str(),
            "PCIE PF Offset",
            "[19:0]",
            svd::Access::ReadWrite,
            None,
        )?);
    }

    regs.extend_from_slice(&[stg_syscfg_82::create()?, stg_syscfg_83::create()?]);

    for idx in 0..2 {
        let bit_off = idx * 32;
        let bit_off_end = bit_off + 31;

        regs.push(create_register_stg_syscfg(
            336 + (idx * 4),
            format!("u0_pcie_pl_sideband_in_{bit_off_end}_{bit_off}").as_str(),
            "PCIE PL Sideband IN (little-endian)",
            "[31:0]",
            svd::Access::ReadWrite,
            None,
        )?);
    }

    for idx in 0..2 {
        let bit_off = idx * 32;
        let bit_off_end = bit_off + 31;

        regs.push(create_register_stg_syscfg(
            344 + (idx * 4),
            format!("u0_pcie_pl_sideband_out_{bit_off_end}_{bit_off}").as_str(),
            "PCIE PL Sideband OUT (little-endian)",
            "[31:0]",
            svd::Access::ReadOnly,
            None,
        )?);
    }

    regs.push(stg_syscfg_88::create()?);

    for idx in 0..2 {
        let bit_off = idx * 32;
        let bit_off_end = bit_off + 31;

        regs.push(create_register_stg_syscfg(
            356 + (idx * 4),
            format!("u0_pcie_test_in_{bit_off_end}_{bit_off}").as_str(),
            "PCIE Test IN (little-endian)",
            "[31:0]",
            svd::Access::ReadWrite,
            None,
        )?);
    }

    for idx in 0..16 {
        let bit_off = idx * 32;
        let bit_off_end = bit_off + 31;

        regs.push(create_register_stg_syscfg(
            364 + (idx * 4),
            format!("u0_pcie_test_out_bridge_{bit_off_end}_{bit_off}").as_str(),
            "PCIE Test OUT Bridge (little-endian)",
            "[31:0]",
            svd::Access::ReadOnly,
            None,
        )?);
    }

    for idx in 0..16 {
        let bit_off = idx * 32;
        let bit_off_end = bit_off + 31;

        regs.push(create_register_stg_syscfg(
            428 + (idx * 4),
            format!("u0_pcie_test_out_{bit_off_end}_{bit_off}").as_str(),
            "PCIE Test OUT (little-endian)",
            "[31:0]",
            svd::Access::ReadOnly,
            None,
        )?);
    }

    regs.extend_from_slice(&[
        stg_syscfg_123::create()?,
        stg_syscfg_124::create()?,
        stg_syscfg_125::create()?,
    ]);

    for idx in 0..8 {
        let bit_off = idx * 32;
        let bit_off_end = bit_off + 31;

        regs.push(create_register_stg_syscfg(
            504 + (idx * 4),
            format!("u1_pcie_axi4_mst0_aratomap_{bit_off_end}_{bit_off}").as_str(),
            "PCIE AXI4 MST0 ARATOMAP (little-endian)",
            "[31:0]",
            svd::Access::ReadOnly,
            None,
        )?);
    }

    regs.extend_from_slice(&[
        stg_syscfg_134::create()?,
        stg_syscfg_135::create()?,
        stg_syscfg_136::create()?,
        stg_syscfg_137::create()?,
        stg_syscfg_138::create()?,
        stg_syscfg_139::create()?,
        stg_syscfg_140::create()?,
        stg_syscfg_141::create()?,
    ]);

    for idx in 0..8 {
        let bit_off = idx * 32;
        let bit_off_end = bit_off + 31;

        regs.push(create_register_stg_syscfg(
            568 + (idx * 4),
            format!("u1_pcie_axi4_slv0_aratomop_{bit_off_end}_{bit_off}").as_str(),
            "PCIE AXI4 ARATOMOP SLV0 (little-endian)",
            "[31:0]",
            svd::Access::ReadWrite,
            None,
        )?);
    }

    regs.extend_from_slice(&[
        stg_syscfg_150::create()?,
        stg_syscfg_151::create()?,
        stg_syscfg_152::create()?,
        stg_syscfg_153::create()?,
        stg_syscfg_154::create()?,
        stg_syscfg_155::create()?,
        stg_syscfg_156::create()?,
        stg_syscfg_157::create()?,
    ]);

    for idx in 0..26 {
        let bit_off = idx * 32;
        let bit_off_end = bit_off + 31;

        regs.push(create_register_stg_syscfg(
            632 + (idx * 4),
            format!("u1_pcie_k_phyparam_{bit_off_end}_{bit_off}").as_str(),
            "PCIE PHY Parameter (little-endian)",
            "[31:0]",
            svd::Access::ReadWrite,
            None,
        )?);
    }

    regs.extend_from_slice(&[
        stg_syscfg_184::create()?,
        stg_syscfg_185::create()?,
        stg_syscfg_186::create()?,
    ]);

    for idx in 0..3 {
        regs.push(create_register_stg_syscfg(
            748 + (idx * 4),
            format!("u1_pcie_pf{idx}_offset").as_str(),
            "PCIE PF Offset",
            "[19:0]",
            svd::Access::ReadWrite,
            None,
        )?);
    }

    regs.extend_from_slice(&[stg_syscfg_190::create()?, stg_syscfg_191::create()?]);

    for idx in 0..2 {
        let bit_off = idx * 32;
        let bit_off_end = bit_off + 31;

        regs.push(create_register_stg_syscfg(
            768 + (idx * 4),
            format!("u1_pcie_pl_sideband_in_{bit_off_end}_{bit_off}").as_str(),
            "PCIE PL Sideband IN (little-endian)",
            "[31:0]",
            svd::Access::ReadWrite,
            None,
        )?);
    }

    for idx in 0..2 {
        let bit_off = idx * 32;
        let bit_off_end = bit_off + 31;

        regs.push(create_register_stg_syscfg(
            776 + (idx * 4),
            format!("u1_pcie_pl_sideband_out_{bit_off_end}_{bit_off}").as_str(),
            "PCIE PL Sideband OUT (little-endian)",
            "[31:0]",
            svd::Access::ReadOnly,
            None,
        )?);
    }

    regs.push(stg_syscfg_196::create()?);

    for idx in 0..2 {
        let bit_off = idx * 32;
        let bit_off_end = bit_off + 31;

        regs.push(create_register_stg_syscfg(
            788 + (idx * 4),
            format!("u1_pcie_test_in_{bit_off_end}_{bit_off}").as_str(),
            "PCIE Test IN (little-endian)",
            "[31:0]",
            svd::Access::ReadWrite,
            None,
        )?);
    }

    for idx in 0..16 {
        let bit_off = idx * 32;
        let bit_off_end = bit_off + 31;

        regs.push(create_register_stg_syscfg(
            796 + (idx * 4),
            format!("u1_pcie_test_out_bridge_{bit_off_end}_{bit_off}").as_str(),
            "PCIE Test OUT Bridge (little-endian)",
            "[31:0]",
            svd::Access::ReadOnly,
            None,
        )?);
    }

    for idx in 0..16 {
        let bit_off = idx * 32;
        let bit_off_end = bit_off + 31;

        regs.push(create_register_stg_syscfg(
            860 + (idx * 4),
            format!("u1_pcie_test_out_{bit_off_end}_{bit_off}").as_str(),
            "PCIE Test OUT (little-endian)",
            "[31:0]",
            svd::Access::ReadOnly,
            None,
        )?);
    }

    regs.push(stg_syscfg_231::create()?);
    regs.push(stg_syscfg_232::create()?);
    regs.push(stg_syscfg_233::create()?);

    Ok(regs)
}

/// Creates a StarFive JH7110 STG Syscon SYSCFG register.
pub fn create_register_stg_syscfg(
    addr_offset: u32,
    field_name: &str,
    field_desc: &str,
    bit_range: &str,
    access: svd::Access,
    default: Option<u64>,
) -> Result<svd::RegisterCluster> {
    let range = create_bit_range(bit_range)?;

    let field = match access {
        svd::Access::ReadWrite | svd::Access::WriteOnly if range.width > 1 => {
            create_field_constraint(
                field_name,
                field_desc,
                range,
                access,
                create_write_constraint(0, (1u64 << range.width) - 1)?,
                None,
            )?
        }
        _ => create_field(field_name, field_desc, range, access, None)?,
    };

    Ok(svd::RegisterCluster::Register(create_register(
        format!("stg_syscfg_{}", addr_offset / 4).as_str(),
        format!("STG SYSCONSAIF SYSCFG {addr_offset}").as_str(),
        addr_offset,
        create_register_properties(32, default.unwrap_or(0))?,
        Some(&[field]),
        None,
    )?))
}
