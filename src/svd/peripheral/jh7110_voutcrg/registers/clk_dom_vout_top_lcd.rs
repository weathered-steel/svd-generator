use crate::svd::{create_enum_value, jh7110};
use crate::Result;

/// Creates StarFive JH7110 VOUTCRG Clock DOM VOUT Top LCD register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg_mux_sel_enum(
        "clk_dom_vout_top_lcd",
        "Clock DOM VOUT Top LCD",
        0x24,
        "clk_dc8200_pix0_out, clk_dc8200_pix1_out",
        &[
            create_enum_value(
                "clk_dc8200_pix0_out",
                "Select `clk_dc8200_pix0_out` as the DOM VOUT Top LCD clock.",
                0b0,
            )?,
            create_enum_value(
                "clk_dc8200_pix1_out",
                "Select `clk_dc8200_pix1_out` as the DOM VOUT Top LCD clock.",
                0b1,
            )?,
        ],
        None,
        None,
    )
}
