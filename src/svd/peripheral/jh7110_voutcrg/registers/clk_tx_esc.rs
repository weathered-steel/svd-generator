use crate::svd::register::jh7110;
use crate::Result;

/// Creates StarFive JH7110 VOUTCRG Clock Transmit Escape register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg(
        "clk_tx_esc",
        "Clock Transmit Escape",
        0xc,
        [31, 12, 10, 12],
        None,
    )
}
