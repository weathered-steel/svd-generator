use crate::svd::register::jh7110;
use crate::Result;

/// Creates StarFive JH7110 VOUTCRG Clock U0 MIPI Transmit DPHY TXESC register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg(
        "clk_u0_mipitx_dphy_txesc",
        "Clock U0 MIPI Transmit DPHY Transmit Escape",
        0x38,
        None,
        None,
    )
}
