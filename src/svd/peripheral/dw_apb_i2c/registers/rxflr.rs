use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the DesignWare I2C RX Failure register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "rxflr",
        "DesignWare I2C RX Failure",
        0x78,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "rxflr",
            "",
            create_bit_range("[31:0]")?,
            svd::Access::ReadOnly,
            None,
        )?]),
        None,
    )?))
}
