use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates the DesignWare I2C SAR register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "sar",
        "DesignWare I2C SAR",
        0x08,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "address_7bit",
                "Slave address, 7-bit mode",
                create_bit_range("[6:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7f)?,
                None,
            )?,
            create_field_constraint(
                "address_10bit",
                "Slave address, 10-bit mode",
                create_bit_range("[9:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3ff)?,
                None,
            )?,
        ]),
        None,
    )?))
}
