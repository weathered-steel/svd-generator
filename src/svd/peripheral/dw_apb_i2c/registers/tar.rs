use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_constraint,
    create_field_enum, create_register, create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates the DesignWare I2C TAR register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "tar",
        "DesignWare I2C TAR",
        0x04,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "address_7bit",
                "Target address, 7-bit mode",
                create_bit_range("[6:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7f)?,
                None,
            )?,
            create_field_constraint(
                "address_10bit",
                "Target address, 10-bit mode",
                create_bit_range("[9:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3ff)?,
                None,
            )?,
            create_field_enum(
                "mode",
                "Target addressing mode - 0: 7-bit, 1: 10-bit",
                create_bit_range("[12:12]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("seven_bit", "7-bit address mode", 0b0)?,
                    create_enum_value("ten_bit", "10-bit address mode", 0b1)?,
                ])?],
                None,
            )?,
        ]),
        None,
    )?))
}
