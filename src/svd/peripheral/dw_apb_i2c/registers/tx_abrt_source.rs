use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the DesignWare I2C TX Abort Source register.
///
/// From linux i2c-designware-core:
///
/// ```no_build,no_run
/// Only expected abort codes are listed here
/// Refer to the datasheet for the full list
/// ```
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "tx_abrt_source",
        "DesignWare I2C TX Abort Source",
        0x80,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "b7_addr_noack",
                "",
                create_bit_range("[0:0]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "b10_addr1_noack",
                "",
                create_bit_range("[1:1]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "b10_addr2_noack",
                "",
                create_bit_range("[2:2]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "txdata_noack",
                "",
                create_bit_range("[3:3]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "gcall_noack",
                "",
                create_bit_range("[4:4]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "gcall_read",
                "",
                create_bit_range("[5:5]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "sbyte_ackdet",
                "",
                create_bit_range("[7:7]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "sbyte_norstrt",
                "",
                create_bit_range("[9:9]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "b10_rd_norstrt",
                "",
                create_bit_range("[10:10]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "master_dis",
                "",
                create_bit_range("[11:11]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "arb_lost",
                "",
                create_bit_range("[12:12]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "slave_flush_txfifo",
                "",
                create_bit_range("[13:13]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "slave_arblost",
                "",
                create_bit_range("[14:14]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "slave_rd_intx",
                "",
                create_bit_range("[15:15]")?,
                svd::Access::ReadOnly,
                None,
            )?,
        ]),
        None,
    )?))
}
