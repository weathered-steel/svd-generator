use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates Synopsys DesignWare MMC Reset register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "rst_n",
        "MMC Reset",
        0x78,
        create_register_properties(32, 0)?,
        Some(&[create_field_enum(
            "rst_n",
            "MMC Reset",
            create_bit_range("[0:0]")?,
            svd::Access::ReadWrite,
            &[create_enum_values(&[
                create_enum_value("hwinactive", "MMC hardware inactive", 0b0)?,
                create_enum_value("hwactive", "MMC hardware active", 0b1)?,
            ])?],
            Some(
                svd::DimElement::builder()
                    .dim(32)
                    .dim_increment(1)
                    .build(svd::ValidateLevel::Strict)?,
            ),
        )?]),
        None,
    )?))
}
