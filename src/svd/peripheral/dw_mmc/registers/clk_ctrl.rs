use crate::svd::create_cluster;
use crate::Result;

pub mod clk;
pub mod clken;

/// Creates Synopsys DesignWare MMC Clock Divider register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "clk_ctrl",
        "MMC Clock Control registers",
        0x8,
        &[clk::create()?, clken::create()?],
        None,
    )?))
}
