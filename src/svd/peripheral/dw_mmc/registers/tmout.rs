use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates Synopsys DesignWare MMC Timeout register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "tmout",
        "MMC Timeout",
        0x14,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "resp",
                "MMC Response Timeout",
                create_bit_range("[7:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xff)?,
                None,
            )?,
            create_field_constraint(
                "data",
                "MMC Data Timeout",
                create_bit_range("[31:8]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xff_ffff)?,
                None,
            )?,
        ]),
        None,
    )?))
}
