use super::int::create_int_fields;
use crate::svd::{create_register, create_register_properties};
use crate::Result;

/// Creates Synopsys DesignWare MMC MINT Status register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "mintsts",
        "MMC MINT status",
        0x40,
        create_register_properties(32, 0)?,
        Some(&[create_int_fields(
            "MMC MINT status",
            svd::Access::ReadWrite,
        )?]),
        None,
    )?))
}
