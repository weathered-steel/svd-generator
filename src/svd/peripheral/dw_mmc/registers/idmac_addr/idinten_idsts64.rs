use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Synopsys DesignWare MMC Internal DMAC Interrupt Enable / DB Address register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "idinten_idsts64",
        "MMC internal DMAC interrupt enable / status - HCON[ADDR_CONFIG] 32-bit(0): interrupt enable, HCON[ADDR_CONFIG] 64-bit(1): status",
        0x8,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "idinten_idsts64",
                "MMC Internal DMAC interrupt enable / status",
                create_bit_range("[31:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
