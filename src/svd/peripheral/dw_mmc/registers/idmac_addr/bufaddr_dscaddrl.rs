use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Synopsys DesignWare MMC Internal DMAC Interrupt Enable / DB Address register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "bufaddr_dscaddrl",
        "MMC internal DMAC buffer address / DSC address - HCON[ADDR_CONFIG] 32-bit(0): buffer address, HCON[ADDR_CONFIG] 64-bit(1): DSC address lower 32-bits",
        0x10,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "bufaddr_dscaddrl",
                "MMC Internal DMAC buffer address / DSC address",
                create_bit_range("[31:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
