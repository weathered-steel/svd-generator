use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates Synopsys DesignWare MMC Clock Divider/Source register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "clk",
        "MMC Clock Configuration - 0: clkdiv, 1: clksrc",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "clk",
            "MMC Clock Configuration",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff)?,
            None,
        )?]),
        Some(
            svd::DimElement::builder()
                .dim(2)
                .dim_increment(0x4)
                .dim_index(Some([String::from("div"), String::from("src")].into()))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
