use crate::svd::peripheral::dw_mmc::DwMmcVersion;
use crate::svd::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::{Error, Result};

const EXPECTED_VER: DwMmcVersion = DwMmcVersion::Version280a;

/// Creates Synopsys DesignWare MMC Internal DMAC Card Detect Threshold Control register definitions.
pub fn create(version: DwMmcVersion) -> Result<svd::RegisterCluster> {
    match version {
        DwMmcVersion::Version280a => Ok(svd::RegisterCluster::Register(create_register(
            "cdthrctl",
            "MMC card detect threshold control",
            0x100,
            create_register_properties(32, 0)?,
            Some(&[
                create_field(
                    "rd_thr_en",
                    "MMC card detect read threshold enable",
                    create_bit_range("[0:0]")?,
                    svd::Access::ReadWrite,
                    None,
                )?,
                create_field(
                    "wr_thr_en",
                    "MMC card detect write threshold enable",
                    create_bit_range("[2:2]")?,
                    svd::Access::ReadWrite,
                    None,
                )?,
                create_field_constraint(
                    "thld",
                    "MMC card detect threshold",
                    create_bit_range("[27:16]")?,
                    svd::Access::ReadWrite,
                    create_write_constraint(0, 0xfff)?,
                    None,
                )?,
            ]),
            None,
        )?)),
        _ => Err(Error::Svd(format!(
            "invalid DesignWare MMC version, have: {version}, expected: {EXPECTED_VER}"
        ))),
    }
}
