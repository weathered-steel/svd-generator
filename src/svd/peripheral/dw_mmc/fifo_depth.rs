use crate::{Error, Result};

const DEPTH_16: usize = 16;
const DEPTH_32: usize = 32;
const DEPTH_64: usize = 64;

/// Represents the FIFO depth size in bits.
///
/// The depth is the bit width of the FIFO register.
#[repr(usize)]
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum DwMmcFifoDepth {
    /// Each FIFO register is 16-bits wide.
    Depth16 = DEPTH_16,
    /// Each FIFO register is 32-bits wide.
    Depth32 = DEPTH_32,
    /// Each FIFO register is 64-bits wide.
    Depth64 = DEPTH_64,
}

impl DwMmcFifoDepth {
    /// Creates a new [DwMmcFifoDepth].
    pub const fn new() -> Self {
        Self::Depth32
    }

    /// Attempts to convert a [`usize`] into a [DwMmcFifoDepth].
    pub const fn from_bits(val: usize) -> Result<Self> {
        match val {
            DEPTH_16 => Ok(Self::Depth16),
            DEPTH_32 => Ok(Self::Depth32),
            DEPTH_64 => Ok(Self::Depth64),
            _ => Err(Error::invalid_variant("dw_mmc::fifo_depth", val)),
        }
    }

    /// Converts a [DwMmcFifoDepth] into a [`usize`].
    pub const fn into_bits(self) -> usize {
        self as usize
    }
}

impl TryFrom<usize> for DwMmcFifoDepth {
    type Error = Error;

    fn try_from(val: usize) -> Result<Self> {
        Self::from_bits(val)
    }
}

impl From<DwMmcFifoDepth> for usize {
    fn from(val: DwMmcFifoDepth) -> Self {
        val.into_bits()
    }
}

impl Default for DwMmcFifoDepth {
    fn default() -> Self {
        Self::new()
    }
}
