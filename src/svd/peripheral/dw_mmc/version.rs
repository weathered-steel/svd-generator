use std::fmt;

/// Represents the implementation model for Synopsys DesignWare MMC peripherals.
#[repr(C)]
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum DwMmcVersion {
    Pre240a = 0,
    Version240a = 0x240a,
    Version280a = 0x280a,
}

impl DwMmcVersion {
    /// Creates a new [DwMmcVersion].
    pub const fn new() -> Self {
        Self::Pre240a
    }

    /// Gets the FIFO data offset by [DwMmcVersion].
    pub const fn data_offset(&self) -> u32 {
        match self {
            Self::Pre240a => 0x100,
            _ => 0x200,
        }
    }
}

impl Default for DwMmcVersion {
    fn default() -> Self {
        Self::new()
    }
}

impl From<&DwMmcVersion> for &'static str {
    fn from(val: &DwMmcVersion) -> Self {
        match val {
            DwMmcVersion::Pre240a => "pre-2.40a",
            DwMmcVersion::Version240a => "2.40a",
            DwMmcVersion::Version280a => "2.80a",
        }
    }
}

impl From<DwMmcVersion> for &'static str {
    fn from(val: DwMmcVersion) -> Self {
        (&val).into()
    }
}

impl fmt::Display for DwMmcVersion {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", <&str>::from(self))
    }
}
