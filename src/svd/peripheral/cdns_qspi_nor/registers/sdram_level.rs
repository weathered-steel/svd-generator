use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the Cadence QSPI NOR Remap Address register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "sdram_level",
        "Cadence QSPI SDRAM Level",
        0x2c,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "rd",
                "SDRAM Read Level",
                create_bit_range("[15:0]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "wr",
                "SDRAM Write Level",
                create_bit_range("[31:16]")?,
                svd::Access::ReadOnly,
                None,
            )?,
        ]),
        None,
    )?))
}
