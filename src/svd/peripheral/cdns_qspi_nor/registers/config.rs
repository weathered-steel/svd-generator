use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_constraint,
    create_field_enum, create_register, create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates the Cadence QSPI NOR Configuration register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "config",
        "Cadence QSPI Configuration",
        0x00,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "enable",
                "Enable the QSPI controller",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "enb_dir_acc_ctrl",
                "Enable direct access controller",
                create_bit_range("[7:7]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "decode",
                "Enable the QSPI decoder",
                create_bit_range("[9:9]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_enum(
                "chipselect",
                "Chip select - CS0: 0b1110, CS1: 0b1101, CS2: 0b1011, CS3: 0b0111",
                create_bit_range("[13:10]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("cs0", "Chip select 0", 0b1110)?,
                    create_enum_value("cs1", "Chip select 1", 0b1101)?,
                    create_enum_value("cs2", "Chip select 2", 0b1011)?,
                    create_enum_value("cs3", "Chip select 3", 0b0111)?,
                ])?],
                None,
            )?,
            create_field(
                "dma",
                "Enable Direct Memory Access",
                create_bit_range("[15:15]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "baud",
                "Set the QSPI BAUD rate divisor",
                create_bit_range("[22:19]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field(
                "dtr_proto",
                "Enable DTR Protocol",
                create_bit_range("[24:24]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "dual_opcode",
                "Enable Dual Opcode Mode",
                create_bit_range("[30:30]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "idle",
                "Set Idle",
                create_bit_range("[31:31]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
