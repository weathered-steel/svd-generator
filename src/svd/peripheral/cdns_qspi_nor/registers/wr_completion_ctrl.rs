use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the Cadence QSPI NOR Remap Address register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "wr_completion_ctrl",
        "Cadence QSPI Write Completion Control",
        0x38,
        create_register_properties(32, 0)?,
        Some(&[
        // FIXME: fill out the remaining control fields. This is the only field in the Linux driver.
            create_field(
                "disable_auto_poll",
                "SPI NAND flashes require the address of the status register to be passed in the Read SR command. Also, some SPI NOR flashes like the Cypress Semper flash expect a 4-byte dummy address in the Read SR command in DTR mode. But this controller does not support address phase in the Read SR command when doing auto-HW polling. So, disable write completion polling on the controller's side. spi-nand and spi-nor will take care of polling the status register.",
                create_bit_range("[14:14]")?,
                svd::Access::ReadWrite,
                None)?,
        ]),
        None,
    )?))
}
