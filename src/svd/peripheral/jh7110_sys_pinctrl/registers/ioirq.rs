use crate::svd::register::{
    create_bit_range, create_cluster, create_default_register, create_enum_value,
    create_enum_values, create_field_enum, create_register, create_register_properties,
};
use crate::Result;

/// Creates a StarFive JH7110 SYS Pinctrl GPO DOEN register.
pub fn create() -> Result<svd::RegisterCluster> {
    const IOIRQ_LEN: usize = 17;
    let desc_fn_fd_acc: [(&str, &str, &str, svd::Access, svd::EnumeratedValues); 8] = [
        (
            "GPIO Interrupt Edge Trigger Selector",
            "is",
            "1: Edge trigger, 0: Level trigger",
            svd::Access::ReadWrite,
            create_enum_values(&[
                create_enum_value("level", "GPIO interrupt trigger selector: level", 0)?,
                create_enum_value("edge", "GPIO interrupt trigger selector: edge", 1)?,
            ])?,
        ),
        (
            "GPIO Interrupt Clear",
            "ic",
            "1: Do not clear the register, 0: Clear the register",
            svd::Access::ReadWrite,
            create_enum_values(&[
                create_enum_value("clear", "GPIO interrupt clear", 0)?,
                create_enum_value("not_clear", "GPIO interrupt do not clear", 1)?,
            ])?,
        ),
        (
            "GPIO Interrupt Both Edge Trigger Selector",
            "ibe",
            "1: Trigger on both edges, 0: Trigger on a single edge",
            svd::Access::ReadWrite,
            create_enum_values(&[
                create_enum_value(
                    "single_edge",
                    "GPIO interrupt trigger edge selector: single edge",
                    0,
                )?,
                create_enum_value(
                    "both_edges",
                    "GPIO interrupt trigger edge selector: both edges",
                    1,
                )?,
            ])?,
        ),
        (
            "GPIO Interrupt Edge Value",
            "iev",
            "1: Positive/Low, 0: Negative/High",
            svd::Access::ReadWrite,
            create_enum_values(&[
                create_enum_value(
                    "negative_high",
                    "GPIO interrupt edge value: negative/high",
                    0,
                )?,
                create_enum_value("positive_low", "GPIO interrupt edge value: positive/low", 1)?,
            ])?,
        ),
        (
            "GPIO Interrupt Edge Mask Selector",
            "ie",
            "1: Unmask, 0: Mask",
            svd::Access::ReadWrite,
            create_enum_values(&[
                create_enum_value("mask", "GPIO interrupt edge mask: mask", 0)?,
                create_enum_value("unmask", "GPIO interrupt edge mask: unmask", 1)?,
            ])?,
        ),
        (
            "GPIO Register Interrupt Status",
            "ris",
            "Status of the edge trigger. The register can be cleared by writing gpio ic",
            svd::Access::ReadOnly,
            create_enum_values(&[
                create_enum_value("clear", "GPIO raw interrupt status: clear", 0)?,
                create_enum_value("set", "GPIO raw interrupt status: set", 1)?,
            ])?,
        ),
        (
            "GPIO Masked Interrupt Status",
            "mis",
            "The masked GPIO IRQ status",
            svd::Access::ReadOnly,
            create_enum_values(&[
                create_enum_value("clear", "GPIO masked interrupt status: clear", 0)?,
                create_enum_value("set", "GPIO masked interrupt status: set", 1)?,
            ])?,
        ),
        (
            "GPIO Synchronization Status",
            "in_sync2_",
            "Status of the gpio_in after synchronization",
            svd::Access::ReadOnly,
            create_enum_values(&[
                create_enum_value(
                    "not_synced",
                    "GPIO status of gpio_in after synchonization: not synced",
                    0,
                )?,
                create_enum_value(
                    "synced",
                    "GPIO status of gpio_in after synchonization: synced",
                    1,
                )?,
            ])?,
        ),
    ];

    let default_reg = create_default_register()?;

    let mut regs = array![default_reg.clone(); IOIRQ_LEN];

    regs[0] = create_register(
        "ioirq0",
        "Enable IRQ function",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[create_field_enum(
            "gpen0",
            "1: Enable, 0: Disable",
            create_bit_range("[0:0]")?,
            svd::Access::ReadWrite,
            &[create_enum_values(&[
                create_enum_value("disable", "GPIO IRQ function: disable", 0)?,
                create_enum_value("enable", "GPIO IRQ function: enable", 1)?,
            ])?],
            None,
        )?]),
        None,
    )
    .map(svd::RegisterCluster::Register)?;

    for (idx, (reg_chunks, (desc, field_name, field_desc, access, enum_vals))) in regs[1..]
        .chunks_exact_mut(2)
        .zip(desc_fn_fd_acc)
        .enumerate()
    {
        let addr_idx0 = (0x4 + (idx * 8)) as u32;
        let addr_idx1 = addr_idx0 + 4;

        let ioirq_idx0 = (idx * 2) + 1;
        let ioirq_idx1 = ioirq_idx0 + 1;

        let enum_values = &[enum_vals];
        match idx {
            0..5 => {
                reg_chunks[0] = create_register(
                    format!("ioirq{ioirq_idx0}").as_str(),
                    format!("SYS IOMUX CFGSAIF SYSCFG IOIRQ {addr_idx0}: {desc}").as_str(),
                    addr_idx0,
                    create_register_properties(32, 0)?,
                    Some(&[create_field_enum(
                        format!("{field_name}0").as_str(),
                        field_desc,
                        create_bit_range("[0:0]")?,
                        access,
                        enum_values,
                        None,
                    )?]),
                    None,
                )
                .map(svd::RegisterCluster::Register)?;

                reg_chunks[1] = create_register(
                    format!("ioirq{ioirq_idx1}").as_str(),
                    format!("SYS IOMUX CFGSAIF SYSCFG IOIRQ {addr_idx1}: {desc}").as_str(),
                    addr_idx1,
                    create_register_properties(32, 0)?,
                    Some(&[create_field_enum(
                        format!("{field_name}1").as_str(),
                        field_desc,
                        create_bit_range("[0:0]")?,
                        access,
                        enum_values,
                        None,
                    )?]),
                    None,
                )
                .map(svd::RegisterCluster::Register)?;
            }
            _ => {
                reg_chunks[0] = create_register(
                    format!("ioirq{ioirq_idx0}").as_str(),
                    format!("SYS IOMUX CFGSAIF SYSCFG IOIRQ {addr_idx0}: {desc}").as_str(),
                    addr_idx0,
                    create_register_properties(32, 0)?,
                    Some(&[create_field_enum(
                        format!("{field_name}0").as_str(),
                        field_desc,
                        create_bit_range("[0:0]")?,
                        access,
                        enum_values,
                        Some(
                            svd::DimElement::builder()
                                .dim(32)
                                .dim_increment(1)
                                .dim_index(Some((0..32).map(|s| format!("_{s}")).collect()))
                                .build(svd::ValidateLevel::Strict)?,
                        ),
                    )?]),
                    None,
                )
                .map(svd::RegisterCluster::Register)?;

                reg_chunks[1] = create_register(
                    format!("ioirq{ioirq_idx1}").as_str(),
                    format!("SYS IOMUX CFGSAIF SYSCFG IOIRQ {addr_idx1}: {desc}").as_str(),
                    addr_idx1,
                    create_register_properties(32, 0)?,
                    Some(&[create_field_enum(
                        format!("{field_name}1").as_str(),
                        field_desc,
                        create_bit_range("[0:0]")?,
                        access,
                        enum_values,
                        Some(
                            svd::DimElement::builder()
                                .dim(32)
                                .dim_increment(1)
                                .dim_index(Some((0..32).map(|s| format!("_{s}")).collect()))
                                .build(svd::ValidateLevel::Strict)?,
                        ),
                    )?]),
                    None,
                )
                .map(svd::RegisterCluster::Register)?;
            }
        }
    }

    create_cluster(
        "ioirq",
        "GPIO Interrupt Request configuration",
        0xdc,
        regs.as_ref(),
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
