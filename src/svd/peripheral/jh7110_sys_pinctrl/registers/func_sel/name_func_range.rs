use super::FuncSel;

/// Convenience wrapper for field name, function selector, and bit range tuple.
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct NameFuncRange {
    pub name: String,
    pub func: FuncSel,
    pub range: svd::BitRange,
    pub constraint: svd::WriteConstraint,
}

impl NameFuncRange {
    /// Creates a new [NameFuncRange].
    pub const fn new() -> Self {
        Self {
            name: String::new(),
            func: FuncSel::new(),
            range: svd::BitRange {
                offset: 0,
                width: 0,
                range_type: svd::BitRangeType::BitRange,
            },
            constraint: svd::WriteConstraint::Range(svd::WriteConstraintRange { min: 0, max: 0 }),
        }
    }
}

impl Default for NameFuncRange {
    fn default() -> Self {
        Self::new()
    }
}
