use crate::svd::register::{
    create_cluster, create_field_constraint, create_register, create_register_properties,
};
use crate::Result;

pub mod func_sel0;
pub mod func_sel1;
pub mod func_sel2;
pub mod func_sel3;
pub mod func_sel4;
pub mod func_sel5;
pub mod func_sel6;

mod name_func_range;
mod select;

pub use name_func_range::*;
pub use select::*;

/// Creates a StarFive JH7110 SYS Pinctrl Function Selector registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "func_sel",
        "Registers used to configure the function selector of the system signal indicated by the register name.",
        0x29c,
        &[
            func_sel0::create()?,
            func_sel1::create()?,
            func_sel2::create()?,
            func_sel3::create()?,
            func_sel4::create()?,
            func_sel5::create()?,
            func_sel6::create()?,
        ],
        None,
    ).map(svd::RegisterCluster::Cluster)
}

fn create_register_func_sel<const N: usize>(
    idx: u32,
    nfra: [NameFuncRange; N],
) -> Result<svd::RegisterCluster> {
    let fields = nfra.map(|nfr| {
        create_field_constraint(
            nfr.name.as_str(),
            nfr.func.into(),
            nfr.range,
            svd::Access::ReadWrite,
            nfr.constraint,
            None,
        )
        .unwrap()
    });

    let addr = idx * 4;

    create_register(
        format!("func_sel{idx}").as_str(),
        format!("SYS IOMUX CFG SAIF SYSCFG Function Selector {addr}").as_str(),
        addr,
        create_register_properties(32, 0)?,
        Some(fields.as_ref()),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
