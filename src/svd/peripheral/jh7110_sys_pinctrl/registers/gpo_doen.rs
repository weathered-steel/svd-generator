use crate::svd::register::{
    create_bit_range, create_cluster, create_default_register, create_field_constraint,
    create_register, create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates a StarFive JH7110 SYS Pinctrl GPO DOEN register.
pub fn create() -> Result<svd::RegisterCluster> {
    const RESET_LEN: usize = 16;
    #[rustfmt::skip]
    const RESET_VALUES: [u64; RESET_LEN] = [
        //       0           1           2           3
        0x08010101, 0x00010001, 0x07010100, 0x00000101,
        //       4           5           6           7
        0x01000000, 0x00000000, 0x00000000, 0x00000000,
        //       8           9          10          11
        0x00000000, 0x23220605, 0x01000001, 0x01000001,
        //      12          13          14          15
        0x0e010d0d, 0x1d011c1c, 0x25012424, 0x29012828,
    ];
    const FIELD_DESC: &str = "The register value indicates the selected GPIO (Output Enable) OEN index from GPIO OEN list 0-49. See Table 2-41: GPIO OEN List for SYS_IOMUX (on page 97) for more information.";

    let default_reg = create_default_register()?;

    let mut regs = array![default_reg.clone(); RESET_LEN];

    for (idx, (reg, rst)) in regs.iter_mut().zip(RESET_VALUES).enumerate() {
        // addr serves as the address offset, and the GPIO number
        //
        // This is a coincidence, since there are 4 GPIOs per register.
        let addr = (idx * 4) as u32;

        let (a, b, c, d) = (addr, addr + 1, addr + 2, addr + 3);

        *reg = create_register(
            format!("gpo_doen{idx}").as_str(),
            format!("SYS IOMUX CFG SAIF SYSCFG FMUX GPIO {a}-{d} DOEN").as_str(),
            addr,
            create_register_properties(32, rst)?,
            Some(&[
                create_field_constraint(
                    format!("doen{a}").as_str(),
                    format!("The selected OEN signal for GPIO{a}. {FIELD_DESC}").as_str(),
                    create_bit_range("[5:0]")?,
                    svd::Access::ReadWrite,
                    create_write_constraint(0, 0x3f)?,
                    None,
                )?,
                create_field_constraint(
                    format!("doen{b}").as_str(),
                    format!("The selected OEN signal for GPIO{b}. {FIELD_DESC}").as_str(),
                    create_bit_range("[13:8]")?,
                    svd::Access::ReadWrite,
                    create_write_constraint(0, 0x3f)?,
                    None,
                )?,
                create_field_constraint(
                    format!("doen{c}").as_str(),
                    format!("The selected OEN signal for GPIO{c}. {FIELD_DESC}").as_str(),
                    create_bit_range("[21:16]")?,
                    svd::Access::ReadWrite,
                    create_write_constraint(0, 0x3f)?,
                    None,
                )?,
                create_field_constraint(
                    format!("doen{d}").as_str(),
                    format!("The selected OEN signal for GPIO{d}. {FIELD_DESC}").as_str(),
                    create_bit_range("[29:24]")?,
                    svd::Access::ReadWrite,
                    create_write_constraint(0, 0x3f)?,
                    None,
                )?,
            ]),
            None,
        )
        .map(svd::RegisterCluster::Register)?;
    }

    create_cluster("gpo_doen", FIELD_DESC, 0x0, regs.as_ref(), None)
        .map(svd::RegisterCluster::Cluster)
}
