use crate::Result;

pub mod fmux_gpen;
pub mod fmux_gpi;
pub mod fmux_gpo_doen;
pub mod fmux_gpo_dout;
pub mod gmac0;
pub mod ioirq;
pub mod ioirq_status;
pub mod osc;
pub mod rgpio;
pub mod rstn;
pub mod rtc;
pub mod testen;

/// Creates StarFive JH7110 AON Pinctrl (compatible) register definitions.
pub fn create() -> Result<Vec<svd::RegisterCluster>> {
    Ok([
        fmux_gpo_doen::create()?,
        fmux_gpo_dout::create()?,
        fmux_gpi::create()?,
        fmux_gpen::create()?,
        ioirq::create()?,
        ioirq_status::create()?,
        testen::create()?,
        rgpio::create()?,
        rstn::create()?,
        rtc::create()?,
        osc::create()?,
        gmac0::create()?,
    ]
    .into())
}
