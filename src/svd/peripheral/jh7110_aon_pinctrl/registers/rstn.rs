use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates the StarFive JH7110 AON Pinctrl RSTN Configuration register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "rstn",
        "AON IOMUX CFG SAIF SYSCFG 68 - RSTN",
        0x44,
        create_register_properties(32, 0x1)?,
        Some(&[
            create_field_enum(
                "smt",
                "Active high Schmitt (SMT) trigger selector - 0: No hysteresis, 1: Schmitt trigger enabled",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("disabled", "Active high Schmitt (SMT) trigger is configured for no hysteresis", 0)?,
                    create_enum_value("enabled", "Active high Schmitt (SMT) trigger is enabled", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "pos",
                "Power-on-Start (POS) enabler - 1: Enable active pull-down for loss of core power, 0: Active pull-down capability disabled",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("disabled", "Power-on-Start active pull-down is disabled", 0)?,
                    create_enum_value("enabled", "Power-on-Start active pull-down is enabled", 1)?,
                ])?],
                None,
            )?,
        ]),
        None,
    )?))
}
