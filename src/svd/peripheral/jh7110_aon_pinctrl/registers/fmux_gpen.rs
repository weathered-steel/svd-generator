use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the StarFive JH7110 AON Pinctrl FMUX3 (GPEN) Configuration register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "fmux_gpen",
        "Enable always-on GPIO IRQ function.",
        0xc,
        create_register_properties(32, 0x0)?,
        Some(&[create_field(
            "gpen0",
            "Enable GPIO IRQ function.",
            create_bit_range("[0:0]")?,
            svd::Access::ReadWrite,
            None,
        )?]),
        None,
    )?))
}
