use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 STG CRG USB APP 125 Clock register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("clk_usb_app125", "Clock USB APP 125", 0x18, None, None)
}
