use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 STG CRG SEC AHB Clock register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("clk_sec_ahb", "Clock SEC AHB", 0x40, None, None)
}
