use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 STG CRG USB STB Clock register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg_divcfg(
        "clk_usb_stb",
        "Clock USB STB",
        0x14,
        [4, 4, 4, 4],
        None,
        None,
    )
}
