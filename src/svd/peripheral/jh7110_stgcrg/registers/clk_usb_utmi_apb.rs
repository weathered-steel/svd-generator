use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 STG CRG USB UTMI APB Clock register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("clk_usb_utmi_apb", "Clock USB UTMI APB", 0x8, None, None)
}
