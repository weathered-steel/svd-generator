use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 STG CRG HIFI4 Clock register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("clk_hifi4_core", "Clock HIFI4 Core", 0x0, None, None)
}
