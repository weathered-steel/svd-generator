use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 STG CRG USB REFCLK Clock register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg(
        "clk_usb_refclk",
        "Clock USB REFCLK",
        0x1c,
        [2, 2, 2, 2],
        None,
    )
}
