use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 STG CRG DMA AXI Clock register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("clk_dma_axi", "Clock DMA AXI", 0x6c, None, None)
}
