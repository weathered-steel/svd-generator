use crate::Result;

pub mod cache_control;
pub mod event_control;
pub mod event_counter;

/// Creates SiFive U74(MC) L2 Performance Monitor register definitions.
pub fn create() -> Result<Vec<svd::RegisterCluster>> {
    Ok([
        cache_control::create()?,
        event_control::create()?,
        event_counter::create()?,
    ]
    .into())
}
