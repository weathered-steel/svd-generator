use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates SiFive U74(MC) L2 Performance Monitor Event Counter registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "event_counter",
        "L2PM Event Control Event Select configuration.",
        0x3000,
        create_register_properties(64, 0)?,
        Some(&[create_field(
            "counter",
            "L2PM Event Counter.",
            create_bit_range("[63:0]")?,
            svd::Access::ReadOnly,
            None,
        )?]),
        Some(
            svd::DimElement::builder()
                .dim(6)
                .dim_increment(8)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
    .map(svd::RegisterCluster::Register)
}
