use crate::svd::create_cluster;
use crate::Result;

pub mod addr;
pub mod count;
pub mod reserved;

/// Creates SiFive U74(MC) L2 Performance Monitor L2 Cache Control Directory ECC registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "ecc",
        "L2 Cache Control Directory ECC registers.\n\nTypes: `dir_fix`: ECC directory fixes, `dir_fail`: ECC directory failures, `data_fix`: ECC data fixes, `data_fail`: ECC data failures.",
        0x100,
        &[
            addr::create()?,
            count::create()?,
            reserved::create()?,
        ],
        Some(svd::DimElement::builder()
            .dim(4)
            .dim_increment(0x20)
            .dim_index(Some([
                "_dir_fix",
                "_dir_fail",
                "_data_fix",
                "_data_fail",
            ].map(String::from).into()))
            .build(svd::ValidateLevel::Strict)?),
    ).map(svd::RegisterCluster::Cluster)
}
