use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates SiFive U74(MC) L2 Performance Monitor L2 Cache Control Way Enable register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "way_enable",
        "L2 Cache Control Way Enable register.\n\nDetermines which ways of the Level 2 Cache Controller are enabled as cache.",
        0x8,
        create_register_properties(32, 0x0)?,
        Some(&[
            create_field_constraint(
                "way_enable",
                "The index of the largest way which has been enabled. May only be increased.",
                create_bit_range("[7:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xff)?,
                None,
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
