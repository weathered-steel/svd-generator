use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_enum,
    create_register, create_register_properties,
};
use crate::Result;

/// Creates SiFive U74(MC) L2 Performance Monitor L2 Cache Control ECC Error Injection register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "ecc_inject_error",
        "L2 Cache Control ECC Error Injection register.\n\nCan be used to insert an ECC error into either the backing data or metadata SRAM.",
        0x40,
        create_register_properties(32, 0x0)?,
        Some(&[
            create_field(
                "ecc_toggle_bit",
                "Toggle (corrupt) this bit index on the next cache operation.",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                Some(svd::DimElement::builder()
                    .dim(8)
                    .dim_increment(1)
                    .build(svd::ValidateLevel::Strict)?),
            )?,
            create_field_enum(
                "ecc_toggle_type",
                "Toggle (corrupt) this bit index on the next cache operation.",
                create_bit_range("[16:16]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("data", "ECC `data` type corruption.", 0)?,
                    create_enum_value("directory", "ECC `directory` type corruption.", 1)?,
                ])?],
                None,
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
