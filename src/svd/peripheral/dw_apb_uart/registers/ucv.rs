use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the UART Component Version.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(
        create_register(
            "ucv",
            "UART Component Version: This register is only valid when the DW_apb_uart is configured to have additional features implemented (ADDITIONAL_FEATURES == YES). If additional features are not implemented, this register does not exist and reading from this register address returns zero.",
            0xf8,
            create_register_properties(32, 0)?,
            Some(&[
                create_field(
                    "ucv",
                    "ASCII value for each number in the version, followed by *. For example 32_30_31_2A represents the version 2.01*",
                    create_bit_range("[31:0]")?,
                    svd::Access::ReadOnly,
                    None,
                )?,
            ]),
            None,
        )?
    ))
}
