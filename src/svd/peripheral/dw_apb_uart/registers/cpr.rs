use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_enum,
    create_register, create_register_properties,
};
use crate::Result;

/// Creates the Component Parameter Register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(
        create_register(
            "cpr",
            "Component Parameter Register: This register is only valid when the DW_apb_uart is configured to have the Component Parameter register implemented (UART_ADD_ENCODED_PARAMS == YES). If the Component Parameter register is not implemented, this register does not exist and reading from this register address returns zero.",
            0xf4,
            create_register_properties(32, 0)?,
            Some(&[
                create_field_enum(
                    "fifo_mode",
                    "0x00 = 0 0x01 = 16 0x02 = 32 to 0x80 = 2048 0x81 - 0xff = reserved",
                    create_bit_range("[23:16]")?,
                    svd::Access::ReadOnly,
                    &[create_enum_values(&[
                        create_enum_value("bits0", "0-bit FIFO mode", 0x00)?,
                        create_enum_value("bits16", "16-bit FIFO mode", 0x01)?,
                        create_enum_value("bits32", "32-bit FIFO mode", 0x02)?,
                        create_enum_value("bits2048", "2048-bit FIFO mode", 0x80)?,
                    ])?],
                    None,
                )?,
                create_field(
                    "dma_extra",
                    "0 = false 1 = true",
                    create_bit_range("[13:13]")?,
                    svd::Access::ReadOnly,
                    None,
                )?,
                create_field(
                    "uart_add_encoded_params",
                    "0 = false 1 = true",
                    create_bit_range("[12:12]")?,
                    svd::Access::ReadOnly,
                    None,
                )?,
                create_field(
                    "shadow",
                    "0 = false 1 = true",
                    create_bit_range("[11:11]")?,
                    svd::Access::ReadOnly,
                    None,
                )?,
                create_field(
                    "fifo_stat",
                    "0 = false 1 = true",
                    create_bit_range("[10:10]")?,
                    svd::Access::ReadOnly,
                    None,
                )?,
                create_field(
                    "fifo_access",
                    "0 = false 1 = true",
                    create_bit_range("[9:9]")?,
                    svd::Access::ReadOnly,
                    None,
                )?,
                create_field(
                    "additional_feat",
                    "0 = false 1 = true",
                    create_bit_range("[8:8]")?,
                    svd::Access::ReadOnly,
                    None,
                )?,
                create_field(
                    "sir_lp_mode",
                    "0 = false 1 = true",
                    create_bit_range("[7:7]")?,
                    svd::Access::ReadOnly,
                    None,
                )?,
                create_field(
                    "sir_mode",
                    "0 = false 1 = true",
                    create_bit_range("[6:6]")?,
                    svd::Access::ReadOnly,
                    None,
                )?,
                create_field(
                    "thre_mode",
                    "0 = false 1 = true",
                    create_bit_range("[5:5]")?,
                    svd::Access::ReadOnly,
                    None,
                )?,
                create_field(
                    "afce_mode",
                    "0 = false 1 = true",
                    create_bit_range("[4:4]")?,
                    svd::Access::ReadOnly,
                    None,
                )?,
                create_field_enum(
                    "apb_data_width",
                    "00 = 8 bits 01 = 16 bits 10 = 32 bits 11 = reserved",
                    create_bit_range("[1:0]")?,
                    svd::Access::ReadOnly,
                    &[create_enum_values(&[
                        create_enum_value("bits8", "8-bit data width", 0b00)?,
                        create_enum_value("bits16", "16-bit data width", 0b01)?,
                        create_enum_value("bits32", "32-bit data width", 0b10)?,
                    ])?],
                    None,
                )?,
            ]),
            None
        )?
    ))
}
