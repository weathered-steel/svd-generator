use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates the Interrupt Identity Register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(
        create_register(
            "iir",
            "Interrupt Identity Register",
            0x8,
            create_register_properties(32, 0x1)?,
            Some(&[
                create_field_enum(
                    "fifose",
                    "FIFOs Enabled. This is used to indicate whether the FIFOs are enabled or disabled. 00 = disabled 11 = enabled",
                    create_bit_range("[7:6]")?,
                    svd::Access::ReadOnly,
                    &[create_enum_values(&[
                        create_enum_value("disabled", "FIFOs are disabled", 0b00)?,
                        create_enum_value("enabled", "FIFOs are enabled", 0b11)?,
                    ])?],
                    None,
                )?,
                create_field_enum(
                    "iid",
                    "Interrupt ID. This indicates the highest priority pending interrupt which can be one of the following types: 0000 = modem status 0001 = no interrupt pending 0010 = THR empty 0100 = received data available 0110 = receiver line status 0111 = busy detect 1100 = character timeout The interrupt priorities are split into four levels that are detailed in Table 8 on page 97. Bit 3 indicates an interrupt can only occur when the FIFOs are enabled and used to distinguish a Character Timeout condition interrupt.",
                    create_bit_range("[3:0]")?,
                    svd::Access::ReadOnly,
                    &[create_enum_values(&[
                        create_enum_value("modem_status", "Modem status interrupt pending", 0b0000)?,
                        create_enum_value("none_pending", "No interrupt pending", 0b0001)?,
                        create_enum_value("thr_empty", "THR empty", 0b0010)?,
                        create_enum_value("data_available", "Received data available", 0b0100)?,
                        create_enum_value("receiver_line_status", "Receiver line status", 0b0110)?,
                        create_enum_value("busy_detect", "Busy detect", 0b0111)?,
                        create_enum_value("character_timeout", "Character timeout", 0b1100)?,
                    ])?],
                    None,
                )?,
            ]),
            None,
        )?
    ))
}
