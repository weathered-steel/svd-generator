use crate::svd::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates the Shadow register cluster.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "shadow",
        "This register is only valid when the DW_apb_uart is configured to have additional shadow registers implemented (SHADOW == YES).",
        0x30,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "srbr",
                "Shadow Receive Buffer Register: This register is only valid when the DW_apb_uart is configured to have additional shadow registers implemented (SHADOW == YES). If shadow registers are not implemented, this register does not exist and reading from this register address returns zero.This is a shadow register for the RBR and has been allocated sixteen 32-bit locations so as to accommodate burst accesses from the master. This register contains the data byte received on the serial input port (sin) in UART mode or the serial infrared input (sir_in) in infrared mode. The data in this register is valid only if the Data Ready (DR) bit in the Line status Register (LSR) is set. If in non-FIFO mode (FIFO_MODE == NONE) or FIFOs are disabled (FCR[0] set to zero), the data in the RBR must be read before the next data arrives, otherwise it is overwritten, resulting in an overrun error. If in FIFO mode (FIFO_MODE != NONE) and FIFOs are enabled (FCR[0] set to one), this register accesses the head of the receive FIFO. If the receive FIFO is full and this register is not read before the next data character arrives, then the data already in the FIFO are preserved, but any incoming data is lost. An overrun error also occurs.",
                create_bit_range("[7:0]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field_constraint(
                "sthr",
                "Shadow Threshold Register: This is a shadow register for the THR and has been allocated sixteen 32-bit locations so as to accommodate burst accesses from the master. This register contains data to be transmitted on the serial output port (sout) in UART mode or the serial infrared output (sir_out_n) in infrared mode. Data should only be written to the THR when the THR Empty (THRE) bit (LSR[5]) is set. If in non-FIFO mode or FIFOs are disabled (FCR[0] set to zero) and THRE is set, writing a single character to the THR clears the THRE. Any additional writes to the THR before the THRE is set again causes the THR data to be overwritten. If in FIFO mode and FIFOs are enabled (FCR[0] set to one) and THRE is set, x number of characters of data may be written to the THR before the FIFO is full. The number x (default=16) is determined by the value of FIFO Depth that you set during configuration. Any attempt to write data when the FIFO is full results in the write data being lost.",
                create_bit_range("[7:0]")?,
                svd::Access::WriteOnly,
                create_write_constraint(0, 0xff)?,
                None,
            )?,
        ]),
        Some(svd::DimElement::builder()
            .dim(16)
            .dim_increment(4)
            .build(svd::ValidateLevel::Strict)?),
    )?))
}
