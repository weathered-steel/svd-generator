use crate::Result;

pub mod cpr;
pub mod ctr;
pub mod dlh;
pub mod dll;
pub mod dmasa;
pub mod far;
pub mod fcr;
pub mod htx;
pub mod ier;
pub mod iir;
pub mod lcr;
pub mod lpdlh;
pub mod lpdll;
pub mod lsr;
pub mod mcr;
pub mod msr;
pub mod rbr;
pub mod rfl;
pub mod rfw;
pub mod sbcr;
pub mod scr;
pub mod sdmam;
pub mod sfe;
pub mod shadow;
pub mod srr;
pub mod srt;
pub mod srts;
pub mod stet;
pub mod tfl;
pub mod tfr;
pub mod thr;
pub mod ucv;
pub mod usr;

/// Creates Synopsys DesignWare APB UART register definitions.
pub fn create() -> Result<Vec<svd::RegisterCluster>> {
    Ok([
        rbr::create()?,
        thr::create()?,
        dll::create()?,
        dlh::create()?,
        ier::create()?,
        iir::create()?,
        fcr::create()?,
        lcr::create()?,
        mcr::create()?,
        lsr::create()?,
        msr::create()?,
        scr::create()?,
        lpdll::create()?,
        lpdlh::create()?,
        shadow::create()?,
        far::create()?,
        tfr::create()?,
        rfw::create()?,
        usr::create()?,
        tfl::create()?,
        rfl::create()?,
        srr::create()?,
        srts::create()?,
        sbcr::create()?,
        sdmam::create()?,
        sfe::create()?,
        srt::create()?,
        stet::create()?,
        htx::create()?,
        dmasa::create()?,
        cpr::create()?,
        ucv::create()?,
        ctr::create()?,
    ]
    .into())
}
