use crate::svd::register::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates ARM PL080 DMA Controller Test Control register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "itcr",
        "DMA Test Control register - enables you to test the DMAC using TIC block-level tests and Built-In Self-Test (BIST) integration and system level tests.",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_enum(
                "test",
                "Test mode enable - multiplex the test registers to control the input and output lines.",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("normal", "DMA test mode disabled, normal operation", 0)?,
                    create_enum_value("test", "DMA test registers multiplexed onto inputs and outputs", 1)?,
                ])?],
                None,
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
