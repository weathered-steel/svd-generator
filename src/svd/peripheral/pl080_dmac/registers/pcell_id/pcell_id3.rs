use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates ARM PL080 DMA Controller Primecell ID 3 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "pcell_id3",
        "DMA PrimeCell ID 3 register - is hard-coded and the fields in the register determine the reset value.",
        0xc,
        create_register_properties(32, 0xB1)?,
        Some(&[
            create_field(
                "pcell_id",
                "These bits read back as 0xB1",
                create_bit_range("[7:0]")?,
                svd::Access::ReadOnly,
                None,
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
