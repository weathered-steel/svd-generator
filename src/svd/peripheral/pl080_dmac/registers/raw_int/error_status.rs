use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates ARM PL080 DMA Controller Raw Interrupt Error Status register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "error_status",
        "Raw Error Interrupt Status Register - indicates the DMA channels that are requesting an error interrupt prior to masking. A HIGH bit indicates that the error interrupt request is active prior to masking.",
        0x4,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_enum(
                "error_status",
                "Status of the error interrupt prior to masking.",
                create_bit_range("[0:0]")?,
                svd::Access::ReadOnly,
                &[create_enum_values(&[
                    create_enum_value("clear", "Error status interrupt is clear", 0)?,
                    create_enum_value("active", "Error status interrupt is active", 1)?,
                ])?],
                Some(svd::DimElement::builder()
                    .dim(8)
                    .dim_increment(1)
                    .build(svd::ValidateLevel::Strict)?),
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
