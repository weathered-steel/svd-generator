use crate::svd::register::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates ARM PL080 DMA Controller Channel Destination Address register defintion.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "dst_addr",
        "DMA Destination Address register - contain the current destination address, byte-aligned, of the data to be transferred. Software programs each register directly before the channel is enabled. When the DMA channel is enabled, the register is updated as the destination address is incremented and by following the linked list when a complete packet of data has been transferred. Reading the register when the channel is active does not provide useful information. This is because by the time the software has processed the value read, the channel might have progressed. It is intended to be read-only when a channel has stopped. In this case, it shows the destination address of the last item read.",
        0x4,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "dst_addr",
                "DMA destination address.",
                create_bit_range("[31:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xffff_ffff)?,
                None,
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
