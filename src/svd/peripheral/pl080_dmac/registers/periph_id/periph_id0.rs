use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates ARM PL080 DMA Controller Peripheral ID 0 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "periph_id0",
        "DMA Peripheral ID 0 register - is hard-coded and the fields in the register determine the reset value.",
        0x0,
        create_register_properties(32, 0x80)?,
        Some(&[
            create_field(
                "part_number0",
                "These bits read back as 0x80",
                create_bit_range("[7:0]")?,
                svd::Access::ReadOnly,
                None,
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
