use crate::Result;

pub mod channel;
pub mod configuration;
pub mod enbld_chns;
pub mod int;
pub mod pcell_id;
pub mod periph_id;
pub mod raw_int;
pub mod soft;
pub mod sync;
pub mod test;

/// Creates ARM PL080 DMA Controller peripheral register definitions.
///
/// Register definitions based on the PL080 technical specification: <https://developer.arm.com/documentation/ddi0196/latest/>
pub fn create() -> Result<Vec<svd::RegisterCluster>> {
    Ok([
        int::create()?,
        raw_int::create()?,
        enbld_chns::create()?,
        soft::create()?,
        configuration::create()?,
        sync::create()?,
        channel::create()?,
        test::create()?,
        periph_id::create()?,
        pcell_id::create()?,
    ]
    .into())
}
