use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_constraint,
    create_field_enum, create_register, create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates the StarFive JH7110 AON Syscon Boot SYSCFG 3 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "aon_syscfg_3",
        "AON SYSCONSAIF SYSCFG 12",
        0xc,
        create_register_properties(32, 0x04d540)?,
        Some(&[
            create_field(
                "u0_boot_ctrl_boot_vector_32_35",
                "Boot vectors 32-35 (little-endian)",
                create_bit_range("[3:0]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "gmac5_axi64_scfg_ram_cfg_slp",
                "SRAM/ROM configuration. SLP: sleep enable, high active, default is low.",
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "gmac5_axi64_scfg_ram_cfg_sd",
                "SRAM/ROM configuration. SD: shutdown enable, high active, default is low.",
                create_bit_range("[5:5]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "gmac5_axi64_scfg_ram_cfg_rtsel",
                "SRAM/ROM configuration. RTSEL: timing setting for debug purpose, default is 2'b01.",
                create_bit_range("[7:6]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field_constraint(
                "gmac5_axi64_scfg_ram_cfg_ptsel",
                "SRAM/ROM configuration. PTSEL: timing setting for debug purpose, default is 2'b01.",
                create_bit_range("[9:8]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field_constraint(
                "gmac5_axi64_scfg_ram_cfg_trb",
                "SRAM/ROM configuration. TRB: timing setting for debug purpose, default is 2'b01.",
                create_bit_range("[11:10]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field_constraint(
                "gmac5_axi64_scfg_ram_cfg_wtsel",
                "SRAM/ROM configuration. WTSEL: timing setting for debug purpose, default is 2'b01.",
                create_bit_range("[13:12]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field(
                "gmac5_axi64_scfg_ram_cfg_vs",
                "SRAM/ROM configuration. VS: timing setting for debug purpose, default is 1'b1.",
                create_bit_range("[14:14]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "gmac5_axi64_scfg_ram_cfg_vg",
                "SRAM/ROM configuration. VG: timing setting for debug purpose, default is 1'b1.",
                create_bit_range("[15:15]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "gmac5_axi64_mac_speed_o",
                "",
                create_bit_range("[17:16]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field_enum(
                "gmac5_axi64_phy_intf_sel_i",
                "Active PHY Selected. When you have multiple GMAC PHY interfaces in your configuration, this field indicates the sampled value of the PHY selector during reset de-assertion. Values: 0x0 - GMII or MII, 0x1 - RGMII, 0x2 - SGMII, 0x3 - TBI, 0x4 - RMII, 0x5 - RTBI, 0x6 - SMII, 0x7 - REVMII",
                create_bit_range("[20:18]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("gmii_mii", "GMII or MII configuration", 0x0)?,
                    create_enum_value("rgmii", "RGMII configuration", 0x1)?,
                    create_enum_value("sgmii", "SGMII configuration", 0x2)?,
                    create_enum_value("tbi", "TBI configuration", 0x3)?,
                    create_enum_value("rmii", "RMII configuration", 0x4)?,
                    create_enum_value("rtbi", "RTBI configuration", 0x5)?,
                    create_enum_value("smii", "SMII configuration", 0x6)?,
                    create_enum_value("revmii", "REVMII configuration", 0x7)?,
                ])?],
                None,
            )?,
        ]),
        None,
    )?))
}
