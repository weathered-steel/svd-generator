use crate::Result;

pub mod clk_dom4_apb_func;
pub mod clk_dvp_inv;
pub mod clk_mipi_rx0_pxl;
pub mod clk_u0_ispv2_top;
pub mod clk_u0_m31dphy_cfgclk_in;
pub mod clk_u0_m31dphy_refclk_in;
pub mod clk_u0_m31dphy_txclkesc_lan0;
pub mod clk_u0_vin_p_axiwr;
pub mod clk_u0_vin_pclk;
pub mod clk_u0_vin_pixel_clk_if;
pub mod clk_u0_vin_sys_clk;
pub mod rst;

/// Creates StarFive JH7110 ISPCRG compatible register definitions.
pub fn create() -> Result<Vec<svd::RegisterCluster>> {
    Ok([
        clk_dom4_apb_func::create()?,
        clk_mipi_rx0_pxl::create()?,
        clk_dvp_inv::create()?,
        clk_u0_m31dphy_cfgclk_in::create()?,
        clk_u0_m31dphy_refclk_in::create()?,
        clk_u0_m31dphy_txclkesc_lan0::create()?,
        clk_u0_vin_pclk::create()?,
        clk_u0_vin_sys_clk::create()?,
        clk_u0_vin_pixel_clk_if::create()?,
        clk_u0_vin_p_axiwr::create()?,
        clk_u0_ispv2_top::create()?,
        rst::create()?,
    ]
    .into())
}
