use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock U0 M31 DPHY Reference In register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg(
        "clk_u0_m31dphy_refclk_in",
        "Clock U0 M31 DPHY Reference In",
        0x10,
        [16, 12, 6, 12],
        None,
    )
}
