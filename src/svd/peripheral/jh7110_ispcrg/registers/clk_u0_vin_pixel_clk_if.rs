use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 ISPCRG Clock Video In Pixel Clock Interface registers.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg(
        "clk_u0_vin_pixel_clk_if",
        "Clock Video In Pixel Clock Interfaces",
        0x20,
        Some(1 << 31),
        Some(
            svd::DimElement::builder()
                .dim(4)
                .dim_increment(0x4)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
}
