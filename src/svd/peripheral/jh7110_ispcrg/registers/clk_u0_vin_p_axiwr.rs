use crate::svd::{create_enum_value, jh7110};
use crate::Result;

/// Creates a StarFive JH7110 ISPCRG Clock Video In P AXIWR register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_mux_sel_enum(
        "clk_u0_vin_p_axiwr",
        "Clock U0 Video In P AXIWR",
        0x30,
        "clk_mipi_rx0_pxl, clk_dvp_inv",
        &[
            create_enum_value(
                "clk_mipi_rx0_pxl",
                "Select `clk_mipi_rx0_pxl` as the U0 Video In P AXIWR clock.",
                0b0,
            )?,
            create_enum_value(
                "clk_dvp_inv",
                "Select `clk_dvp_inv` as the U0 Video In P AXIWR clock.",
                0b1,
            )?,
        ],
        None,
    )
}
