use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 ISPCRG Clock Video In SYSCLK register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg(
        "clk_u0_vin_sys_clk",
        "Clock U0 Video In SYSCLK",
        0x1c,
        [8, 2, 1, 2],
        None,
    )
}
