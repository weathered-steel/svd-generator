use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 ISPCRG Clock Video In PCLK register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg(
        "clk_u0_vin_pclk",
        "Clock U0 Video In PCLK",
        0x18,
        Some(1 << 31),
        None,
    )
}
