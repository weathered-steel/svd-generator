use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock DOM4 APB Function register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg(
        "clk_dom4_apb_func",
        "Clock DOM4 APB Function",
        0x0,
        [15, 6, 6, 6],
        None,
    )
}
