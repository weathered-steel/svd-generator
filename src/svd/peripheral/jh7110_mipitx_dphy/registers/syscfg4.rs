use crate::svd::register::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates StarFive JH7110 MIPI TX SYSCFG 3 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "syscfg4",
        "MIPITX DPHY SYSCFG 4: mipitx_apbifsaif_syscfg_16",
        0x10,
        create_register_properties(32, 0x210)?,
        Some(&[create_field_constraint(
            "rg_cdtx",
            "RG CDTX: u0_mipitx_dphy_RG_CDTX",
            create_bit_range("[4:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0x1f)?,
            Some(
                svd::DimElement::builder()
                    .dim(2)
                    .dim_increment(5)
                    .dim_index(Some(
                        ["_l4n_hstx_res", "_l4p_hstx_res"].map(String::from).into(),
                    ))
                    .build(svd::ValidateLevel::Strict)?,
            ),
        )?]),
        None,
    )?))
}
