use crate::svd::register::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates StarFive JH7110 MIPI TX DPHY SYSCFG 2 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "syscfg2",
        "MIPITX DPHY SYSCFG 2: mipitx_apbifsaif_syscfg_8",
        0x8,
        create_register_properties(32, 0x1080_0000)?,
        Some(&[
            create_field(
                "mposv",
                "MPOSV: u0_mipitx_dphy_MPOSVn",
                create_bit_range("[0:0]")?,
                svd::Access::ReadOnly,
                Some(
                    svd::DimElement::builder()
                        .dim(15)
                        .dim_increment(1)
                        .dim_index(Some(
                            [
                                "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42",
                                "43", "44", "45", "46",
                            ]
                            .map(String::from)
                            .into(),
                        ))
                        .build(svd::ValidateLevel::Strict)?,
                ),
            )?,
            create_field(
                "rgs_cdtx",
                "RGS CDTX: u0_mipitx_dphy_RGS_CDTX",
                create_bit_range("[15:15]")?,
                svd::Access::ReadOnly,
                Some(
                    svd::DimElement::builder()
                        .dim(4)
                        .dim_increment(1)
                        .dim_index(Some(
                            [
                                "_pll_fm_cplt",
                                "_pll_fm_over",
                                "_pll_fm_under",
                                "_pll_unlock",
                            ]
                            .map(String::from)
                            .into(),
                        ))
                        .build(svd::ValidateLevel::Strict)?,
                ),
            )?,
            create_field_constraint(
                "rg_cdtx",
                "RG CDTX: u0_mipitx_dphy_RG_CDTX",
                create_bit_range("[23:19]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x1f)?,
                Some(
                    svd::DimElement::builder()
                        .dim(2)
                        .dim_increment(5)
                        .dim_index(Some(
                            ["_l0n_hstx_res", "_l0p_hstx_res"].map(String::from).into(),
                        ))
                        .build(svd::ValidateLevel::Strict)?,
                ),
            )?,
        ]),
        None,
    )?))
}
