use crate::svd::create_peripheral;
use crate::Result;

pub mod registers;

/// Represents a Synopsys DesignWare AXI DMAC (compatible) peripheral
pub struct DwAxiDmac {
    peripheral: svd::Peripheral,
}

impl DwAxiDmac {
    /// Creates a new [DwAxiDmac] peripheral.
    pub fn create(
        name: &str,
        base_address: u64,
        size: u32,
        interrupt: Option<Vec<svd::Interrupt>>,
        dma_channels: u64,
        dim: u32,
    ) -> Result<Self> {
        let dim_element = if dim > 0 {
            Some(
                svd::DimElement::builder()
                    .dim(dim)
                    .dim_increment(size)
                    .build(svd::ValidateLevel::Strict)?,
            )
        } else {
            None
        };

        let peripheral = create_peripheral(
            name,
            format!("Synopsys DesignWare AXI DMAC: {name}").as_str(),
            base_address,
            size,
            interrupt,
            Some(registers::create(dma_channels)?),
            dim_element,
            Some("DMAC".into()),
        )?;

        Ok(Self { peripheral })
    }

    /// Gets a reference to the SVD [`Peripheral`](svd::Peripheral).
    pub const fn peripheral(&self) -> &svd::Peripheral {
        &self.peripheral
    }

    /// Gets a mutable reference to the SVD [`Peripheral`](svd::Peripheral).
    pub fn peripheral_mut(&mut self) -> &mut svd::Peripheral {
        &mut self.peripheral
    }

    /// Converts the [DwAxiDmac] into the inner SVD [`Peripheral`](svd::Peripheral).
    pub fn to_inner(self) -> svd::Peripheral {
        self.peripheral
    }
}
