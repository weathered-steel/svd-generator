use crate::svd::create_cluster;
use crate::Result;

pub mod apb;
pub mod core;

/// Creates a StarFive JH7110 SYSCRG Clock UART 3-5 registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "uart35",
        "Clock UART U3-U5",
        0x18,
        &[apb::create()?, core::create()?],
        Some(
            svd::DimElement::builder()
                .dim(3)
                .dim_increment(0x8)
                .dim_index(Some((3..6).map(|i| format!("_u{i}")).collect()))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
    .map(svd::RegisterCluster::Cluster)
}
