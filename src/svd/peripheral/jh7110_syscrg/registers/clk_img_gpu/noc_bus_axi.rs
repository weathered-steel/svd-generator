use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG IMG GPU NOC Bus AXI register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg(
        "noc_bus_axi",
        "clk_u0_sft7110_noc_bus_clk_gpu_axi",
        0x14,
        None,
        None,
    )
}
