use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG U7MC Trace COM register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("trace_com", "Clock U7MC Trace", 0x30, Some(1 << 31), None)
}
