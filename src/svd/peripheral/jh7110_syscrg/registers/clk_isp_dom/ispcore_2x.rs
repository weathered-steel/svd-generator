use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG ISP DOM ISPCORE 2X register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg(
        "ispcore_2x",
        "clk_u0_dom_isp_top_clk_dom_isp_top_clk_ispcore_2x",
        0x0,
        None,
        None,
    )
}
