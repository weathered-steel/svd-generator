use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG ISP DOM NOC Bus AXI register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg(
        "noc_bus_axi",
        "clk_u0_sft7110_noc_bus_clk_isp_axi",
        0x8,
        Some(1 << 31),
        None,
    )
}
