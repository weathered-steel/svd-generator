use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG ISP DOM AXI register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg(
        "axi",
        "clk_u0_dom_isp_top_clk_dom_isp_top_clk_isp_axi",
        0x4,
        None,
        None,
    )
}
