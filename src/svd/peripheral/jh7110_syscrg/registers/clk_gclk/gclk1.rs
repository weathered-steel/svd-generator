use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock ISP AXI GCLK1 register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg_divcfg(
        "clk_gclk1",
        "Clock GCLK 1",
        0x4,
        [62, 16, 16, 16],
        Some((1 << 31) | 16),
        None,
    )
}
