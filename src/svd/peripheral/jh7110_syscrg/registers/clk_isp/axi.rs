use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG ISP AXI register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg("axi", "Clock ISP AXI", 0x4, [4, 2, 2, 2], None)
}
