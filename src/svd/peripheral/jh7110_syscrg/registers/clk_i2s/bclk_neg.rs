use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock I2S BCLK NEG register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_clk_polarity("bclk_neg", "Clock I2S BCLK Negative", 0x14, Some(1 << 30))
}
