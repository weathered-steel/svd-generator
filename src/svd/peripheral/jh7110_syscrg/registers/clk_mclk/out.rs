use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG MCLK Out register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("out", "Clock MCLK Out", 0x8, None, None)
}
