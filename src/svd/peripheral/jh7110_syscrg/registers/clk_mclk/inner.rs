use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG MCLK Inner register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg("inner", "Clock MCLK Inner", 0x0, [64, 12, 12, 12], None)
}
