use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG GMAC SRC register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg("gmac_src", "Clock GMAC Source", 0x8, [7, 2, 2, 2], None)
}
