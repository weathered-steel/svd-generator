use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG GMAC GMAC5 AXI64 TXI register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_clk_polarity(
        "gmac5_axi64_txi",
        "Clock GMAC5 AXI64 TX Inverter",
        0x24,
        Some(1 << 30),
    )
}
