use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Software RESET Address Selector SYSCRG Status RESET 2 register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_rst_sel(
        "rst2",
        "RESET 2",
        0x8,
        2,
        Some(0b1111_1111_1110_0111_1110_1111_1100_1100),
    )
}
