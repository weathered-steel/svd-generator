use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Software RESET Address Selector Software Address Selector RESET 1 register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_rst_sel(
        "rst1",
        "RESET 1",
        0x4,
        1,
        Some(0b0111_1110_0111_1111_1110_0000_0000),
    )
}
