use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Software RESET Address Selector Software Address Selector RESET 3 register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_rst_sel(
        "rst3",
        "RESET 3",
        0xc,
        3,
        Some(0b0111_1111_1111_1111_1111_1111_1111),
    )
}
