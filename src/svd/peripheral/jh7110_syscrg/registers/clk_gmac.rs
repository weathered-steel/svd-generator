use crate::svd::create_cluster;
use crate::Result;

pub mod gmac0_gtx;
pub mod gmac0_gtxc;
pub mod gmac0_ptp;
pub mod gmac1_gtx;
pub mod gmac1_gtxc;
pub mod gmac1_rmii_rtx;
pub mod gmac5_axi64;
pub mod gmac5_axi64_ptp;
pub mod gmac5_axi64_rx;
pub mod gmac5_axi64_rxi;
pub mod gmac5_axi64_tx;
pub mod gmac5_axi64_txi;
pub mod gmac_phy;
pub mod gmac_src;

/// Creates a StarFive JH7110 SYSCRG GMAC registers.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "clk_gmac",
        "Clock GMAC registers",
        0x184,
        &[
            gmac5_axi64::create()?,
            gmac_src::create()?,
            gmac1_gtx::create()?,
            gmac1_rmii_rtx::create()?,
            gmac5_axi64_ptp::create()?,
            gmac5_axi64_rx::create()?,
            gmac5_axi64_rxi::create()?,
            gmac5_axi64_tx::create()?,
            gmac5_axi64_txi::create()?,
            gmac1_gtxc::create()?,
            gmac0_gtx::create()?,
            gmac0_ptp::create()?,
            gmac_phy::create()?,
            gmac0_gtxc::create()?,
        ],
        None,
    )?))
}
