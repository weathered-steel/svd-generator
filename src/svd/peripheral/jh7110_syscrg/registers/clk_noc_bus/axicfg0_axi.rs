use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG NOC Bus CPU AXI register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg(
        "axicfg0_axi",
        "clk_u0_sft7110_noc_bus_clk_axicfg0_axi",
        0x4,
        Some(1 << 31),
        None,
    )
}
