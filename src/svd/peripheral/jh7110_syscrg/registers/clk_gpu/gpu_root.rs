use crate::svd::{create_enum_value, jh7110};
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock GPU Root register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_mux_sel_enum(
        "root",
        "Clock GPU Root",
        0x0,
        "clk_pll2, clk_pll1",
        &[
            create_enum_value("clk_pll2", "Select `clk_pll2` as  the GPU Root clock.", 0b0)?,
            create_enum_value("clk_pll1", "Select `clk_pll1` as  the GPU Root clock.", 0b1)?,
        ],
        None,
    )
}
