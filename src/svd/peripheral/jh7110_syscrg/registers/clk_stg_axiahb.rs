use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock AXI Configuration register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg(
        "clk_stg_axiahb",
        "Clock STG AXI AHB",
        0x20,
        [2, 2, 2, 2],
        None,
    )
}
