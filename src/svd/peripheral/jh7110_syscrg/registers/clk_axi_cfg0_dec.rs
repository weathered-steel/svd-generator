use crate::svd::create_cluster;
use crate::Result;

pub mod dec_main;
pub mod hifi4;
pub mod main_div;

/// Creates a StarFive JH7110 SYSCRG AXI CFG 0 DEC registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "clk_axi_cfg0_dec",
        "Clock AXI CFG 0 DEC registers",
        0x14c,
        &[main_div::create()?, dec_main::create()?, hifi4::create()?],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
