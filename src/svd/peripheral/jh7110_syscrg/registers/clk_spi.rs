use crate::svd::create_cluster;
use crate::Result;

pub mod apb;

/// Creates a StarFive JH7110 SYSCRG Clock SPI registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "clk_spi",
        "Clock SPI registers",
        0x20c,
        &[apb::create()?],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
