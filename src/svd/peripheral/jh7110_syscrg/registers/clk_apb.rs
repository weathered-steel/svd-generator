use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock AXI Configuration register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("clk_apb0", "Clock APB", 0x30, Some(1 << 31), None)
}
