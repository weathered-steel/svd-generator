use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock TDM Internal register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg_divcfg(
        "internal",
        "Clock TDM Internal",
        0x8,
        [61, 1, 1, 1],
        None,
        None,
    )
}
