use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock PDM APB register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("apb", "Clock PDM APB", 0x4, None, None)
}
