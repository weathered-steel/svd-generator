use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock PDM DMIC register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg_divcfg("dmic", "Clock PDM DMIC", 0x0, [64, 8, 8, 8], None, None)
}
