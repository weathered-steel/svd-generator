use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Video Output NOC Display AXI register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("noc_diplay_axi", "Clock NOC Display AXI", 0x8, None, None)
}
