use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG JPEG CODAJ12 APB register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("codaj12_apb", "Clock CODAJ12 APB", 0xc, None, None)
}
