use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG JPEG CODAJ12 Core register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg_divcfg(
        "codaj12_core",
        "Clock CODAJ12 Core",
        0x8,
        [16, 6, 6, 6],
        None,
        None,
    )
}
