use crate::svd::create_cluster;
use crate::Result;

pub mod apb;
pub mod can;
pub mod tim;

/// Creates a StarFive JH7110 SYSCRG Clock Mailbox APB register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "clk_can_ctrl",
        "Clock CAN Controller",
        0x1cc,
        &[apb::create()?, tim::create()?, can::create()?],
        Some(
            svd::DimElement::builder()
                .dim(2)
                .dim_increment(0xc)
                .dim_index(Some(["_u0", "_u1"].map(String::from).into()))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
    .map(svd::RegisterCluster::Cluster)
}
