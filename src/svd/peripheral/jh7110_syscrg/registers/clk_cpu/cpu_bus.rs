use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock CPU Bus register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg("bus", "Clock CPU Bus", 0x8, [2, 2, 2, 2], None)
}
