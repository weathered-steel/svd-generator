use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock PLL1 Divider 8 register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg("clk_pll1_div8", "clk_pll1_div8", 0xa8, [2, 2, 2, 2], None)
}
