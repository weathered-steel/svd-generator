use crate::svd::create_cluster;
use crate::Result;

pub mod cpu_bus;
pub mod cpu_core;
pub mod cpu_root;

/// Creates a StarFive JH7110 SYSCRG Clock CPU registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "clk_cpu",
        "Clock CPU registers",
        0x0,
        &[cpu_root::create()?, cpu_core::create()?, cpu_bus::create()?],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
