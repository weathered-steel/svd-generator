use crate::svd::create_cluster;
use crate::Result;

pub mod codaj12_apb;
pub mod codaj12_axi;
pub mod codaj12_core;
pub mod codec_axi;

/// Creates a StarFive JH7110 SYSCRG JPEG registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "clk_jpeg",
        "Clock CODAJ12 registers",
        0x104,
        &[
            codec_axi::create()?,
            codaj12_axi::create()?,
            codaj12_core::create()?,
            codaj12_apb::create()?,
        ],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
