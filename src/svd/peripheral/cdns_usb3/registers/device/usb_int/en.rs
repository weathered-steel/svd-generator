use super::create_int_fields;
use crate::svd::{create_register, create_register_properties};
use crate::Result;

/// Creates Cadence USB3 Device Global Interrupt Enable register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "en",
        "Global Interrupt Enable.",
        0x0,
        create_register_properties(32, 0x137003f)?,
        Some(&create_int_fields()?),
        None,
    )?))
}
