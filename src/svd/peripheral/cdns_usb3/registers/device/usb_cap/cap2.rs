use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates Cadence USB3 Device Global Capability 2 register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "cap2",
        "USB3 Global capability 2.",
        0x4,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_enum(
                "actual_mem_size",
                "The actual size of the connnected on-chip RAM memory in kB - 0: 256kB, 1-255: 1-255kB.",
                create_bit_range("[7:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&(0..=255).filter_map(|i| {
                    if i == 0 {
                        create_enum_value("mem256kb", "Actual supported memory size: 256kB", i).ok()
                    } else {
                        create_enum_value(&format!("mem{i}kb"), &format!("Actual supported memory size: {i}kB"), i).ok()
                    }
                }).collect::<Vec<svd::EnumeratedValue>>())?],
                None,
            )?,
            create_field_enum(
                "max_mem_size",
                "Max supported memory size.",
                create_bit_range("[11:8]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("mem4kb", "Support for 4kB memory.", 0x8)?,
                    create_enum_value("mem8kb", "Support for 8kB memory.", 0x9)?,
                    create_enum_value("mem16kb", "Support for 16kB memory.", 0xa)?,
                    create_enum_value("mem32kb", "Support for 32kB memory.", 0xb)?,
                    create_enum_value("mem64kb", "Support for 64kB memory.", 0xc)?,
                    create_enum_value("mem128kb", "Support for 128kB memory.", 0xd)?,
                    create_enum_value("mem256kb", "Support for 256kB memory.", 0xe)?,
                ])?],
                None,
            )?,
        ]),
        None,
    )?))
}
