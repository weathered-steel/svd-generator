use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates Cadence USB3 Device Endpoint Upper Address for DMA Operations register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "ep_dma_ext_addr",
        "USB3 Endpoint upper address for DMA operations.",
        0x70,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "ep_dma_ext_addr",
            "Custom packet.",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff)?,
            None,
        )?]),
        None,
    )?))
}
