use crate::svd::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates Cadence USB3 Device Global LPM register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "usb_lpm",
        "Global LPM.",
        0x10,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "hird",
                "Host Initiated Resume Duration.",
                create_bit_range("[3:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field(
                "brw",
                "Remote Wakeup Enable - `bRemoteWake`.",
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
