use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates Cadence USB3 Device DMA AXI Control 0 register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "ctrl0",
        "Device DMA AXI control 0: **WARNING** DMA AXI max burst length - In versions preceding DEV_VER_V2, for example, iMX8QM, there exist the bugs in the DMA. These bugs occur when the trb_burst_size exceeds 16 and the address is not aligned to 128 Bytes (which is a product of the 64-bit AXI and AXI maximum burst length of 16 or 0xF+1, dma_axi_ctrl0[3:0]). This results in data corruption when it crosses the 4K border. The corruption specifically occurs from the position (4K - (address & 0x7F)) to 4K. So force trb_burst_size to 16 at such platform.",
        0xc,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "max_burst",
                "Device DMA AXI max burst length - length = value + 1?",
                create_bit_range("[3:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
        ]),
        None,
    )?))
}
