use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates Cadence USB3 Device DMA AXI Control register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "ctrl",
        "Device DMA AXI control.",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_enum(
                "marprot",
                "DMA memory-access read protection.",
                create_bit_range("[2:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value(
                        "secure",
                        "Protected access to memory - can cause problem with cache",
                        0x0,
                    )?,
                    create_enum_value("non_secure", "Unprotected access to memory", 0x2)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "mawprot",
                "DMA memory-access write protection.",
                create_bit_range("[18:16]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value(
                        "secure",
                        "Protected access to memory - can cause problem with cache",
                        0x0,
                    )?,
                    create_enum_value("non_secure", "Unprotected access to memory", 0x2)?,
                ])?],
                None,
            )?,
        ]),
        None,
    )?))
}
