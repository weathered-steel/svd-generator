use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Cadence USB3 Device Endpoint TDL register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "ep",
        "TDL endpoint configuration.",
        0x8,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "ep_out",
                "Endpoint TDL - OUT.",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                Some(
                    svd::DimElement::builder()
                        .dim(16)
                        .dim_increment(1)
                        .build(svd::ValidateLevel::Strict)?,
                ),
            )?,
            create_field(
                "ep_in",
                "Endpoint TDL - IN.",
                create_bit_range("[16:16]")?,
                svd::Access::ReadWrite,
                Some(
                    svd::DimElement::builder()
                        .dim(16)
                        .dim_increment(1)
                        .build(svd::ValidateLevel::Strict)?,
                ),
            )?,
        ]),
        None,
    )?))
}
