use crate::svd::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates Cadence USB3 Device Global Command register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "usb_cmd",
        "USB3 Global command.",
        0x8,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "set_addr",
                "Set function address",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "faddr",
                "Function address - saves the address to the device only when `set_addr` is set to `1`.",
                create_bit_range("[7:1]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7f)?,
                None,
            )?,
            create_field(
                "sdnfw",
                "Send Function Wake Device Notification TP - used only in SS mode.",
                create_bit_range("[8:8]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "stmode",
                "Set Test Mode - used only in FS/HS mode.",
                create_bit_range("[9:9]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "tmode_sel",
                "Test Mode Selector - used only in FS/HS mode.",
                create_bit_range("[11:10]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0b11)?,
                None,
            )?,
            create_field(
                "sdnltm",
                "Send Latency Tolerance Message Device Notification TP - used only in SS mode.",
                create_bit_range("[12:12]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "spkt",
                "Send Custom Transaction Packet - used only in SS mode.",
                create_bit_range("[13:13]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            // FIXME: the Linux driver defines two bitmasks over the same range, however neither
            // field is used in any driver. Consider dropping this field, as the definition looks
            // very buggy.
            create_field_constraint(
                "dnfw_int_dnltm_belt",
                "Device Notification `Function Wake` Interface Value / Device Notification `Latency Tolerance Message` BELT Value - used only in SS mode.",
                create_bit_range("[23:16]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xff)?,
                None,
            )?,
        ]),
        None,
    )?))
}
