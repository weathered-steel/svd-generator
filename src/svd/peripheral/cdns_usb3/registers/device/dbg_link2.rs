use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Cadence USB3 Device Debug Link 2 register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "dbg_link2",
        "Device debug link 2.",
        0x108,
        create_register_properties(32, 0)?,
        // FIXME: no available public documentation for valid values...
        Some(&[create_field(
            "dbg_link2",
            "Device debug link 2.",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            None,
        )?]),
        None,
    )?))
}
