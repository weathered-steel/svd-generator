use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates Cadence USB3 Device Global Status register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "usb_sts",
        "USB3 Global status.",
        0x4,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_enum(
                "cfgsts",
                "Device configuration status.",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("not_cfg", "Device not configured.", 0b0)?,
                    create_enum_value("cfg", "Device configured.", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "ov",
                "On-chip memory overflow.",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("ok", "On-chip memory status OK.", 0b0)?,
                    create_enum_value("overflow", "On-chip memory overflow.", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "usb3cons",
                "Superspeed connection status.",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("disconnected", "SuperSpeed mode disconnected.", 0b0)?,
                    create_enum_value("connected", "SuperSpeed mode connected.", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "dtrans",
                "DMA transfer configuration status.",
                create_bit_range("[3:3]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("single_request", "Single DMA request.", 0b0)?,
                    create_enum_value("multiple_trb_chain", "Multiple TRB chain.", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "usbspeed",
                "Device speed.",
                create_bit_range("[6:4]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("undefined", "Undefined speed - value after reset.", 0b000)?,
                    create_enum_value("low", "Low speed.", 0b001)?,
                    create_enum_value("full", "Full speed.", 0b010)?,
                    create_enum_value("high", "High speed.", 0b011)?,
                    create_enum_value("super", "Super speed.", 0b100)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "endian",
                "Endianess for SFR access.",
                create_bit_range("[7:7]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("little", "Little Endian order - default after hardware reset", 0b0)?,
                    create_enum_value("big", "Big Endian order", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "clk2off",
                "FS/HS clock turn-off status.",
                create_bit_range("[8:8]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("always_on", "FS/HS clock is always on", 0b0)?,
                    create_enum_value("turn_off", "FS/HS clock turn-off in L2 (FS/HS mode) is enabled - default afteer hardware reset", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "clk3off",
                "PCLK clock turn-off status.",
                create_bit_range("[9:9]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("always_on", "PCLK clock is always on.", 0b0)?,
                    create_enum_value("turn_off", "PCLK clock turn-off in U3 (SS mode) is enabled - default afteer hardware reset.", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "in_rst",
                "Controller in reset state.",
                create_bit_range("[10:10]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("active", "Internal reset is active", 0b0)?,
                    create_enum_value("not_active", "Internal reset is not active and controller is fully operational.", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "tdl_trb_en",
                "Status of the `TDL calculation based on TRB` feature.",
                create_bit_range("[11:11]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("disabled", "TDL TRB calculation disabled.", 0b0)?,
                    create_enum_value("enabled", "TDL TRB calculation enabled.", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "devs",
                "Device enable status.",
                create_bit_range("[14:14]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("disabled", "USB device is disabled - VBUS disconnected from internal logic.", 0b0)?,
                    create_enum_value("enabled", "USB device is enabled - VBUS connected to internal logic.", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "addressed",
                "Address status.",
                create_bit_range("[15:15]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("default", "USB device is in the default state.", 0b0)?,
                    create_enum_value("addressed", "USB device is at least in address state.", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "l1ens",
                "L1 LPM state enable status - used in FS/HS mode.",
                create_bit_range("[16:16]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("disabled", "Entering into L1 LPM state disabled.", 0b0)?,
                    create_enum_value("enabled", "Entering into L1 LPM state enabled.", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "vbuss",
                "Internal VBUS connection status - used both in FS/HS and SS mode.",
                create_bit_range("[17:17]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("not_detected", "Internal VBUS is not detected.", 0b0)?,
                    create_enum_value("detected", "Internal VBUS is detected.", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "lpmst",
                "FS/HS LPM state - used in FS/HS mode.",
                create_bit_range("[19:18]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("l0", "L0 state.", 0b00)?,
                    create_enum_value("l1", "L1 state.", 0b01)?,
                    create_enum_value("l2", "L2 state.", 0b10)?,
                    create_enum_value("l3", "L3 state.", 0b11)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "usb2cons",
                "Disable HS status - used in FS/HS mode.",
                create_bit_range("[20:20]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("disconnect", "The disconnect bit for FS/HS mode is set.", 0b0)?,
                    create_enum_value("connect", "The disconnect bit for FS/HS mode is not set.", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "disable_hs",
                "FS/HS mode connection status - used in FS/HS mode.",
                create_bit_range("[21:21]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("enabled", "High Speed operations in USB2.0 mode are not disabled.", 0b0)?,
                    create_enum_value("disabled", "High Speed operations in USB2.0 mode are disabled.", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "ens_u",
                "U1/2 state enable status - used in SS mode.",
                create_bit_range("[24:24]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("disabled", "Entering to U1/2 state disabled.", 0b0)?,
                    create_enum_value("enabled", "Entering to U1/2 state enabled.", 0b1)?,
                ])?],
                Some(svd::DimElement::builder()
                    .dim(2)
                    .dim_increment(1)
                    .dim_index(Some([
                        String::from("1"),
                        String::from("2"),
                    ].into()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
            create_field_enum(
                "lst",
                "SuperSpeed Link LTSSM state.",
                create_bit_range("[29:26]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("u0", "U0 link status.", 0x0)?,
                    create_enum_value("u1", "U1 link status.", 0x1)?,
                    create_enum_value("u2", "U2 link status.", 0x2)?,
                    create_enum_value("u3", "U3 link status.", 0x3)?,
                    create_enum_value("disabled", "Link disabled.", 0x4)?,
                    create_enum_value("rxdetect", "Link detected receive.", 0x5)?,
                    create_enum_value("inactive", "Link inactive.", 0x6)?,
                    create_enum_value("polling", "Link polling.", 0x7)?,
                    create_enum_value("recovery", "Link is in recovery.", 0x8)?,
                    create_enum_value("hot_reset", "Link is hot reset.", 0x9)?,
                    create_enum_value("comp_mode", "Link is in COMP mode.", 0xa)?,
                    create_enum_value("lb_state", "Link is in LB state.", 0xb)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "dmaoff",
                "DMA clock turn-off status.",
                create_bit_range("[30:30]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("always_on", "DMA clock is always on - default after hardware reset.", 0b0)?,
                    create_enum_value("turn_off", "DMA clock turn-off in U1, U2, and U3 (SS mode) is enabled.", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "endian2",
                "SFR Endian status.",
                create_bit_range("[31:31]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("little", "Little Endian order - default after hardware reset.", 0b0)?,
                    create_enum_value("big", "Big Endian order.", 0b1)?,
                ])?],
                None,
            )?,
        ]),
        None,
    )?))
}
