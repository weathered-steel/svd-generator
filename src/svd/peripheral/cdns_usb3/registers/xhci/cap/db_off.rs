use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Cadence USB3 XHCI Host Controller Doorbell Array Offset register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "db_off",
        "USB3 XHCI host controller doorbell array offset.",
        0x14,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "db_off",
            "USB3 XHCI host controller doorbell array offset.",
            create_bit_range("[31:0]")?,
            svd::Access::ReadOnly,
            None,
        )?]),
        None,
    )?))
}
