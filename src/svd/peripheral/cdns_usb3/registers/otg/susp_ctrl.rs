use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Cadence USB3 OTG Suspend Control register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "susp_ctrl",
        "USB3 OTG suspend control.",
        0x48,
        create_register_properties(32, 0)?,
        // FIXME: no available public documentation for valid values...
        Some(&[create_field(
            "susp_ctrl",
            "USB3 OTG suspend control.",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            None,
        )?]),
        None,
    )?))
}
