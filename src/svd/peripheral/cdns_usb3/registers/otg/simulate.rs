use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Cadence USB3 OTG Simulate register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "simulate",
        "USB3 OTG simulate.",
        0x40,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "power_lost",
            "USB3 OTG simulation - indicates if power was lost before.",
            create_bit_range("[0:0]")?,
            svd::Access::ReadWrite,
            None,
        )?]),
        None,
    )?))
}
