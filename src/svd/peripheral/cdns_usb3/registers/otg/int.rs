use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Cadence USB3 OTG Interrupt register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "int",
        "USB3 OTG interrupt registers - 0: enable, 1: vector status. Write `1` to interrupt vector fields to clear the status.",
        0x20,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "id_change",
                "USB3 OTG ID change interrupt.",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "vbusvalid",
                "USB3 OTG VBUS valid change detected interrupt - 0: rise, 1: fall.",
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                Some(svd::DimElement::builder()
                    .dim(2)
                    .dim_increment(1)
                    .dim_index(Some([
                        String::from("_rise"),
                        String::from("_fall"),
                    ].into()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
        ]),
        Some(svd::DimElement::builder()
            .dim(2)
            .dim_increment(0x4)
            .dim_index(Some([
                String::from("_en"),
                String::from("_vect"),
            ].into()))
            .build(svd::ValidateLevel::Strict)?),
    )?))
}
