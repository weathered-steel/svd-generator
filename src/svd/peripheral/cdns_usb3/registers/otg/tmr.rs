use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates Cadence USB3 OTG Timer register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "tmr",
        "USB3 OTG timer.",
        0x2c,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "tmr",
            "USB3 OTG timer.",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff)?,
            None,
        )?]),
        None,
    )?))
}
