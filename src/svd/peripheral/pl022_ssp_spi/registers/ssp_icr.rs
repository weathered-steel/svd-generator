use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates the SSPICR write-only interrupt clear register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "ssp_icr",
        "The SSPICR register is the interrupt clear register and is write-only. On a write of 1, the corresponding interrupt is cleared. A write of 0 has no effect.",
        0x20,
        create_register_properties(16, 0)?,
        Some(&[
            create_field_enum(
                "roric",
                "Clears the SSPRORINTR interrupt",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("nop", "SSPRORINTR receive interrupt clear: no-op", 0)?,
                    create_enum_value("clear", "SSPRORINTR receive interrupt clear: clear", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "rtic",
                "Clears the SSPRTINTR interrupt",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("nop", "SSPRTINTR transmit interrupt clear: no-op", 0)?,
                    create_enum_value("clear", "SSPRTINTR transmit interrupt clear: clear", 1)?,
                ])?],
                None,
            )?
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
