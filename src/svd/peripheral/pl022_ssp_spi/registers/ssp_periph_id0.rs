use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the SSPPeriphID0 read-only, hard-coded register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "ssp_periph_id0",
        "The SSPPeriphID0 register is hard-coded and the fields within the register determine reset value. The SSPPeriphID0-3 registers are four 8-bit registers, that span address locations 0xFE0 to 0xFEC. The registers can conceptually be treated as a single 32-bit register.",
        0xfe0,
        create_register_properties(16, 0)?,
        Some(&[
            create_field(
                "part_number0",
                "These bits read back as 0x22",
                create_bit_range("[7:0]")?,
                svd::Access::ReadOnly,
                None,
            )?
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
