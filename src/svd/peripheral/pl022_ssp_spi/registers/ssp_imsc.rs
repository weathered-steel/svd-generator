use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates the SSPIMSC interrupt mask set or clear register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "ssp_imsc",
        "The SSPIMSC register is the interrupt mask set or clear register. It is a RW register. On a read this register gives the current value of the mask on the relevant interrupt. A write of 1 to the particular bit sets the mask, enabling the interrupt to be read. A write of 0 clears the corresponding mask. All the bits are cleared to 0 when reset.",
        0x14,
        create_register_properties(16, 0)?,
        Some(&[
            create_field_enum(
                "rorim",
                "Receive overrun interrupt mask",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("masked", "Receive FIFO written to while full condition interrupt is masked", 0)?,
                    create_enum_value("not_masked", "Receive FIFO written to while full condition interrupt is not masked", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "rtim",
                "Receive timeout interrupt mask",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("masked", "Receive FIFO not empty and no read prior to timeout period interrupt is masked", 0)?,
                    create_enum_value("not_masked", "Receive FIFO not empty and no read prior to timeout period interrupt is not masked", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "rxim",
                "Receive FIFO interrupt mask",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("masked", "Receive FIFO half full or less condition interrupt is masked", 0)?,
                    create_enum_value("not_masked", "Receive FIFO half full or less condition interrupt is not masked", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "txim",
                "Transmit FIFO interrupt mask",
                create_bit_range("[3:3]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("masked", "Transmit FIFO half empty or less condition interrupt is masked", 0)?,
                    create_enum_value("not_masked", "Transmit FIFO half empty or less condition interrupt is not masked", 1)?,
                ])?],
                None,
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
