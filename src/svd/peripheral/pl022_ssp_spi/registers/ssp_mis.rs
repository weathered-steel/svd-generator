use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the SSPMIS masked interrupt status register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "ssp_mis",
        "The SSPMIS register is the masked interrupt status register. It is a RO register. On a read this register gives the current masked status value of the corresponding interrupt. A write has no effect.",
        0x1c,
        create_register_properties(16, 0)?,
        Some(&[
            create_field(
                "rormis",
                "Gives the receive over run masked interrupt status, after masking, of the SSPRORINTR interrupt",
                create_bit_range("[0:0]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "rtmis",
                "Gives the receive timeout masked interrupt state, after masking, of the SSPRTINTR interrupt",
                create_bit_range("[1:1]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "rxmis",
                "Gives the receive FIFO masked interrupt state, after masking, of the SSPRXINTR interrupt",
                create_bit_range("[2:2]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "txmis",
                "Gives the transmit FIFO masked interrupt state, after masking, of the SSPTXINTR interrupt",
                create_bit_range("[3:3]")?,
                svd::Access::ReadOnly,
                None,
            )?
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
