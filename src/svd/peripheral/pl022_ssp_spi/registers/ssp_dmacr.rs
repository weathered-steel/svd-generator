use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the SSPDMACR DMA control register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "ssp_dmacr",
        "The SSPDMACR register is the DMA control register. It is a RW register. All the bits are cleared to 0 on reset.",
        0x24,
        create_register_properties(16, 0)?,
        Some(&[
            create_field(
                "rxdmae",
                "Receive DMA Enable. If this bit is set to 1, DMA for the receive FIFO is enabled.",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "txdmae",
                "Transmit DMA Enable. If this bit is set to 1, DMA for the transmit FIFO is enabled.",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
