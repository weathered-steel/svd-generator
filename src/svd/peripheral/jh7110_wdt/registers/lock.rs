use crate::svd::register::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates StarFive JH7110 WDT Lock register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "lock",
        "StarFive JH7110 Watchdog Lock register.",
        0xc00,
        create_register_properties(32, 0xe5331aae)?,
        Some(&[
            create_field_enum(
                "lock",
                "Write magic values to enable/disable writes to other watchdog registers: 0x1acce551: enable writes / unlock, 0xe5331aae: disable writes / lock. Defaults to the 'lock' value.",
                create_bit_range("[31:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("enable_writes", "Enables writes to other watchdog registers", 0x1acc_e551)?,
                    create_enum_value("disable_writes", "Disables writes to other watchdog registers", 0xe533_1aae)?,
                ])?],
                None,
            )?,
        ]),
        None,
    )?))
}
