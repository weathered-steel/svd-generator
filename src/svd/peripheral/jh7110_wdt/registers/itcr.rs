use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates StarFive JH7110 WDT Integration Test Control register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "itcr",
        "StarFive JH7110 Watchdog Integration Test Control register.",
        0xf00,
        create_register_properties(32, 0xe5331aae)?,
        Some(&[create_field(
            "enable",
            "Integration test mode enable - 0: disable, 1: enable",
            create_bit_range("[0:0]")?,
            svd::Access::ReadWrite,
            None,
        )?]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
