use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates StarFive JH7110 WDT Value register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "value",
        "StarFive JH7110 Watchdog Value register.",
        0x4,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "load",
            "Current value for the watchdog counter.",
            create_bit_range("[31:0]")?,
            svd::Access::ReadOnly,
            None,
        )?]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
