use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates the Opencores PTC PWM v1 RPTC_HRC register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "hrc",
        "Opencores PTC PWM v1 HRC register is a 2nd out of two reference/capture registers. It has two functions: - In reference mode it is used to assert high PWM output or to generate an interrupt - In capture mode it captures RPTC_CNTR value on high value of ptc_capt signal. The RPTC_HRC should have lower value than RPTC_LRC. This is because PWM output goes first high and later low.", 
        0x4,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
                "hrc",
                "",
                create_bit_range("[31:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xffff_ffff)?,
                None,
        )?]),
        None,
    ).map(svd::RegisterCluster::Register)
}
