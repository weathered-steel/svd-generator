use crate::svd::{create_derived_peripheral, create_peripheral};
use crate::Result;

pub mod registers;

/// Represents a Synopsys DesignWare APB UART (compatible) peripheral
pub struct DwApbUart {
    peripheral: svd::Peripheral,
}

impl DwApbUart {
    /// Creates a new [DwApbUart] peripheral.
    pub fn create(
        name: &str,
        base_address: u64,
        size: u32,
        interrupt: Option<Vec<svd::Interrupt>>,
        dim: u32,
        count: usize,
    ) -> Result<Self> {
        let full_name = format!("{name}{count}");

        let dim_element = if dim > 0 {
            Some(
                svd::DimElement::builder()
                    .dim(dim)
                    .dim_increment(size)
                    .build(svd::ValidateLevel::Strict)?,
            )
        } else {
            None
        };

        let desc = format!("Synopsys DesignWare APB UART: {full_name}");

        if count == 0 {
            create_peripheral(
                full_name.as_str(),
                desc.as_str(),
                base_address,
                size,
                interrupt,
                Some(registers::create()?),
                dim_element,
                Some("UART".into()),
            )
        } else {
            create_derived_peripheral(
                full_name.as_str(),
                desc.as_str(),
                base_address,
                size,
                interrupt,
                "UART",
                format!("{name}0").as_str(),
            )
        }
        .map(|peripheral| Self { peripheral })
    }

    /// Gets a reference to the SVD [`Peripheral`](svd::Peripheral).
    pub const fn peripheral(&self) -> &svd::Peripheral {
        &self.peripheral
    }

    /// Gets a mutable reference to the SVD [`Peripheral`](svd::Peripheral).
    pub fn peripheral_mut(&mut self) -> &mut svd::Peripheral {
        &mut self.peripheral
    }

    /// Converts the [DwApbUart] into the inner SVD [`Peripheral`](svd::Peripheral).
    pub fn to_inner(self) -> svd::Peripheral {
        self.peripheral
    }
}
