use crate::svd::jh7110;
use crate::Result;

/// Creates StarFive JH7110 AON CRG CLK_OSC register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg("clk_osc", "Oscillator Clock", 0x0, [4, 4, 4, 4], None)
}
