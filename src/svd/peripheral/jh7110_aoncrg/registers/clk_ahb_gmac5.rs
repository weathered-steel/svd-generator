use crate::svd::jh7110;
use crate::Result;

/// Creates StarFive JH7110 AON CRG CLK_AHB_GMAC5 register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("clk_ahb_gmac5", "AHB GMAC5 Clock", 0x8, None, None)
}
