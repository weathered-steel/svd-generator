use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates the RISC-V PLIC Threshold Claim Threshold register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "threshold",
        "Interrupt priority threshold of each context.",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
                "threshold",
                "The PLIC will mask all PLIC interrupts of a priority less than or equal to `threshold`.",
                create_bit_range("[31:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xffff_ffff)?,
                None,
        )?]),
        None,
    ).map(svd::RegisterCluster::Register)
}
