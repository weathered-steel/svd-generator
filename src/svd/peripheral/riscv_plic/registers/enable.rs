use crate::svd::create_cluster;
use crate::Result;

pub mod enable_bits;
pub mod reserved;

/// Creates the RISC-V PLIC Enable register.
pub fn create(harts: usize, interrupts: usize) -> Result<svd::RegisterCluster> {
    const CLUSTER_WIDTH: u32 = 0x80;

    let num_elem = interrupts.saturating_div(32) as u32 + (interrupts % 32 != 0) as u32;
    let elem_width = num_elem.saturating_mul(4);

    let elem_regs: Vec<svd::RegisterCluster> = if elem_width < CLUSTER_WIDTH {
        [enable_bits::create(num_elem)?, reserved::create()?].into()
    } else {
        [enable_bits::create(num_elem)?].into()
    };

    create_cluster(
        "enable",
        "PLIC interrupt enable registers (per-HART)",
        0x2000,
        elem_regs.as_ref(),
        Some(
            svd::DimElement::builder()
                .dim(harts as u32)
                .dim_increment(0x80)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
    .map(svd::RegisterCluster::Cluster)
}
