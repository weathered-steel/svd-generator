use crate::Result;

pub mod csr;
pub mod sec;

/// Creates register definitions for OpenEdges Orbit Memory Controller peripherals (OMC).
pub fn create() -> Result<Vec<svd::RegisterCluster>> {
    Ok([csr::create()?, sec::create()?].into())
}
