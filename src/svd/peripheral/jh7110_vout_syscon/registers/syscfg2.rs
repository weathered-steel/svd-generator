use crate::svd::register::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates the StarFive JH7110 VOUT SYSCFG 2 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "syscfg2",
        "VOUT SYSCFG 2: dom_vout_sysconsaif_8",
        0x8,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_enum(
                "u0_lcd_data_mapping_dp_rgb_fmt",
                "RGB format in DP data - 0: RGB888, 1: RGB666, 2: RGB565",
                create_bit_range("[1:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("rgb888", "RGB format in DP data: RGB888", 0)?,
                    create_enum_value("rgb666", "RGB format in DP data: RGB666", 1)?,
                    create_enum_value("rgb565", "RGB format in DP data: RGB565", 2)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "u0_lcd_data_mapping_dpi_dp_sel",
                "DPI or DP - 0: DPI, 1: DP",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("dpi", "ICD data mapping selection: DPI", 0)?,
                    create_enum_value("dp", "ICD data mapping selection: DP", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "u1_display_panel_sel",
                "DC8200 panel - 0: Panel 0, 1: Panel 1",
                create_bit_range("[3:3]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("panel0", "DC8200 panel: 0", 0)?,
                    create_enum_value("panel1", "DC8200 panel: 1", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "u2_display_panel_sel",
                "DC8200 panel - 0: Panel 0, 1: Panel 1",
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("panel0", "DC8200 panel: 0", 0)?,
                    create_enum_value("panel1", "DC8200 panel: 1", 1)?,
                ])?],
                None,
            )?,
        ]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
