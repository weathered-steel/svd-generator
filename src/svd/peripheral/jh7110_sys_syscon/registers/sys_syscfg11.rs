use crate::svd::register::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_constraint,
    create_field_enum, create_register, create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates a StarFive JH7110 SYS SYSCON SYSCFG 11 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "sys_syscfg11",
        "SYS SYSCONSAIF SYSCFG 44",
        0x2c,
        create_register_properties(32, 0x66_2601)?,
        Some(&[
            create_field_constraint(
                "pll1_prediv",
                "PLL1 prediv value.",
                create_bit_range("[5:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3f)?,
                None,
            )?,
            create_field(
                "pll1_testen",
                "PLL1 test enable.",
                create_bit_range("[6:6]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "pll1_testsel",
                "PLL1 test selector.",
                create_bit_range("[8:7]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field_constraint(
                "pll2_cpi_bias",
                "PLL2 CPI bias.",
                create_bit_range("[11:9]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7)?,
                None,
            )?,
            create_field_constraint(
                "pll2_cpp_bias",
                "PLL2 CPP bias.",
                create_bit_range("[14:12]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7)?,
                None,
            )?,
            create_field_enum(
                "pll2_dacpd",
                "PLL2 DACPD.",
                create_bit_range("[15:15]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("off", "Disable PLL2 DACPD.", 0b0)?,
                    create_enum_value("on", "Enable PLL2 DACPD.", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "pll2_dsmpd",
                "PLL2 DSMPD.",
                create_bit_range("[16:16]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("off", "Disable PLL2 DSMPD.", 0b0)?,
                    create_enum_value("on", "Enable PLL2 DSMPD.", 0b1)?,
                ])?],
                None,
            )?,
            create_field_constraint(
                "pll2_fbdiv",
                "PLL2 fbdiv value.",
                create_bit_range("[28:17]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xfff)?,
                None,
            )?,
        ]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
