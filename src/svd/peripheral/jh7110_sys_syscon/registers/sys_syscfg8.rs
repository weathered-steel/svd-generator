use crate::svd::register::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_constraint,
    create_field_enum, create_register, create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates a StarFive JH7110 SYS SYSCON SYSCFG 8 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "sys_syscfg8",
        "SYS SYSCONSAIF SYSCFG 32",
        0x20,
        create_register_properties(32, 0x5155_5555)?,
        Some(&[
            create_field_constraint(
                "pll0_frac",
                "PLL0 frac value.",
                create_bit_range("[23:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xff_ffff)?,
                None,
            )?,
            create_field_constraint(
                "pll0_gvco_bias",
                "PLL0 GVCO bias.",
                create_bit_range("[25:24]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field(
                "pll0_lock",
                "PLL0 lock.",
                create_bit_range("[26:26]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field_enum(
                "pll0_pd",
                "PLL0 PD enable setting - driving the register low turns PD `on`.",
                create_bit_range("[27:27]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("on", "Enable the PLL0 PD.", 0b0)?,
                    create_enum_value("off", "Disable the PLL0 PD.", 0b1)?,
                ])?],
                None,
            )?,
            create_field_constraint(
                "pll0_postdiv1",
                "PLL0 postdiv1 value.",
                create_bit_range("[29:28]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field_constraint(
                "pll0_postdiv2",
                "PLL0 postdiv2 value.",
                create_bit_range("[31:30]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
        ]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
