use crate::svd::create_cluster;
use crate::Result;

pub mod reset_vector_31_0;
pub mod reset_vector_35_32;

/// Creates a StarFive JH7110 SYS SYSCON SYSCFG Reset Vector 0-35 registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "reset_vector_35_0",
        "Reset vector cluster of 36 vector fields",
        0x0,
        &[reset_vector_31_0::create()?, reset_vector_35_32::create()?],
        Some(
            svd::DimElement::builder()
                .dim(3)
                .dim_increment(0x8)
                .dim_index(Some(["_1", "_2", "_3"].map(String::from).into()))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
    .map(svd::RegisterCluster::Cluster)
}
