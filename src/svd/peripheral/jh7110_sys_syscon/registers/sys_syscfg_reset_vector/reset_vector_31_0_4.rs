use crate::svd::register::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates a StarFive JH7110 SYS SYSCON SYSCFG Reset Vector 0-31 group 4 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "reset_vector_31_0_4",
        "Reset vector register with 32 vector fields",
        0x18,
        create_register_properties(32, 0x2a00_0000)?,
        Some(&[create_field_constraint(
            "vectors",
            "Reset vector bits",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff)?,
            None,
        )?]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
