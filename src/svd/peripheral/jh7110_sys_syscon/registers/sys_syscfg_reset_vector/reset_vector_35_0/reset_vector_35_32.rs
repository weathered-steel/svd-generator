use crate::svd::register::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates a StarFive JH7110 SYS SYSCON SYSCFG Reset Vector 32-35 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "reset_vector_35_32",
        "Reset vector register with 4 vector fields",
        0x4,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "vectors",
            "Reset vector bits",
            create_bit_range("[3:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xf)?,
            None,
        )?]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
