use super::{create_field_noc_bus_oic_evemon, create_fields_noc_bus_oic_evemon};
use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates a StarFive JH7110 SYS SYSCON SYSCFG 14 register.
pub fn create() -> Result<svd::RegisterCluster> {
    let fields: Vec<svd::Field> = [create_field_noc_bus_oic_evemon(
        "trigger",
        6,
        0,
        svd::Access::ReadOnly,
    )?]
    .into_iter()
    .chain((0..5).filter_map(|idx| {
        let bit = 5 + idx;
        create_field(
            format!("noc_bus_oic_ignore_modifiable_{idx}").as_str(),
            "",
            create_bit_range(format!("[{bit}:{bit}]").as_str()).ok()?,
            svd::Access::ReadWrite,
            None,
        )
        .ok()
    }))
    .chain(
        (7..9)
            .filter_map(|idx| {
                Some(
                    create_fields_noc_bus_oic_evemon(idx, 1 + (idx * 2))
                        .ok()?
                        .into_iter(),
                )
            })
            .flatten(),
    )
    .collect();

    create_register(
        "sys_syscfg14",
        "SYS SYSCONSAIF SYSCFG 56",
        0x38,
        create_register_properties(32, 0)?,
        Some(fields.as_ref()),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
