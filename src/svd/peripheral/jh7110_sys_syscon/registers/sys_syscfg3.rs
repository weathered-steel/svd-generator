use crate::svd::register::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates a StarFive JH7110 SYS SYSCON SYSCFG 3 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "sys_syscfg3",
        "SYS SYSCONSAIF SYSCFG 12",
        0xc,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_enum(
                "vout0_remap_awaddr_gpio0",
                "GPIO Group 0 (GPIO21-35) voltage select",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value(
                        "select_33v",
                        "GPIO Group 0 (GPIO21-35) voltage select 3.3V",
                        0,
                    )?,
                    create_enum_value(
                        "select_18v",
                        "GPIO Group 0 (GPIO21-35) voltage select 1.8V",
                        1,
                    )?,
                ])?],
                None,
            )?,
            create_field_enum(
                "vout0_remap_awaddr_gpio1",
                "GPIO Group 1 (GPIO36-63) voltage select",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value(
                        "select_33v",
                        "GPIO Group 1 (GPIO36-63) voltage select 3.3V",
                        0,
                    )?,
                    create_enum_value(
                        "select_18v",
                        "GPIO Group 1 (GPIO36-63) voltage select 1.8V",
                        1,
                    )?,
                ])?],
                None,
            )?,
            create_field_enum(
                "vout0_remap_awaddr_gpio2",
                "GPIO Group 2 (GPIO0-6) voltage select",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value(
                        "select_33v",
                        "GPIO Group 2 (GPIO0-6) voltage select 3.3V",
                        0,
                    )?,
                    create_enum_value(
                        "select_18v",
                        "GPIO Group 2 (GPIO0-6) voltage select 1.8V",
                        1,
                    )?,
                ])?],
                None,
            )?,
            create_field_enum(
                "vout0_remap_awaddr_gpio3",
                "GPIO Group 3 (GPIO7-20) voltage select",
                create_bit_range("[3:3]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value(
                        "select_33v",
                        "GPIO Group 3 (GPIO7-20) voltage select 3.3V",
                        0,
                    )?,
                    create_enum_value(
                        "select_18v",
                        "GPIO Group 3 (GPIO7-20) voltage select 1.8V",
                        1,
                    )?,
                ])?],
                None,
            )?,
        ]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
