use crate::svd::register::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint, jh7110,
};
use crate::Result;

/// Creates a StarFive JH7110 SYS SYSCON SYSCFG 33 register.
pub fn create() -> Result<svd::RegisterCluster> {
    let fields: Vec<svd::Field> = (0..10)
        .filter_map(|idx| match idx {
            0 => create_field_constraint(
                "reset_vector_35_32_4",
                "Reset vector bits",
                create_bit_range("[3:0]").ok()?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf).ok()?,
                None,
            )
            .ok(),
            1..=4 => create_field(
                format!("u0_suppress_fetch{idx}").as_str(),
                "",
                create_bit_range(format!("[{}:{}]", idx + 3, idx + 3).as_str()).ok()?,
                svd::Access::ReadWrite,
                None,
            )
            .ok(),
            _ => create_field(
                format!("u0_wfi_from_tile{}", idx - 5).as_str(),
                "",
                create_bit_range(format!("[{}:{}]", idx + 3, idx + 3).as_str()).ok()?,
                svd::Access::ReadWrite,
                None,
            )
            .ok(),
        })
        .chain(jh7110::create_fields_sram_config(
            "u0_vdec_int_sram_config",
            13,
        )?)
        .collect();

    create_register(
        "sys_syscfg33",
        "SYS SYSCONSAIF SYSCFG 132",
        0x84,
        create_register_properties(32, 0x1aa_8000)?,
        Some(fields.as_ref()),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
