use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates a StarFive JH7110 SYS SYSCON SYSCFG 4 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "sys_syscfg4",
        "SYS SYSCONSAIF SYSCFG 16",
        0x10,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "coda12_cur_inst",
                "Tie 0 in JPU internal, do not care",
                create_bit_range("[1:0]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "wave511_vpu_idle",
                "VPU monitoring signal",
                create_bit_range("[2:2]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "can_ctrl_fd_enable_0",
                "",
                create_bit_range("[3:3]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "can_ctrl_host_ecc_disable_0",
                "",
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "can_ctrl_host_if_0",
                "",
                create_bit_range("[23:5]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "qspi_sclk_dlychain_sel",
                "des_qspi_sclk_dla: clock delay",
                create_bit_range("[28:24]")?,
                svd::Access::ReadOnly,
                None,
            )?,
        ]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
