use crate::svd::register::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates a StarFive JH7110 SYS SYSCON SYSCFG 1 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "sys_syscfg1",
        "SYS SYSCONSAIF SYSCFG 4",
        0x4,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "sd1_remap_awaddr",
                "",
                create_bit_range("[3:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field_constraint(
                "sec_haddr_remap",
                "",
                create_bit_range("[7:4]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field_constraint(
                "usb_araddr_remap",
                "",
                create_bit_range("[11:8]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field_constraint(
                "usb_awaddr_remap",
                "",
                create_bit_range("[15:12]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field_constraint(
                "vdec_remap_awaddr",
                "",
                create_bit_range("[19:16]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field_constraint(
                "venc_remap_araddr",
                "",
                create_bit_range("[23:20]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field_constraint(
                "venc_remap_awaddr",
                "",
                create_bit_range("[27:24]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field_constraint(
                "vout0_remap_araddr",
                "",
                create_bit_range("[31:28]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
        ]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
