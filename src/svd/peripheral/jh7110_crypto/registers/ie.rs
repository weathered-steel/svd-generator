use crate::svd::create_cluster;
use crate::Result;

pub mod flag;
pub mod mask;

/// Creates StarFive JH7110 Crypto Interrupt Enable registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "ie",
        "JH7110 Crypto Interrupt Enable registers",
        0x8,
        &[mask::create()?, flag::create()?],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
