use crate::svd::create_cluster;
use crate::Result;

pub mod caafr;
pub mod caar;
pub mod cacr;
pub mod caefr;
pub mod caer;
pub mod canfr;
pub mod canr;
pub mod casr;
pub mod fifo_counter;

/// Creates StarFive JH7110 Crypto CRYPTO registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "crypto",
        "JH7110 Crypto CRYPTO registers",
        0x400,
        &[
            cacr::create()?,
            casr::create()?,
            caar::create()?,
            caer::create()?,
            canr::create()?,
            caafr::create()?,
            caefr::create()?,
            canfr::create()?,
            fifo_counter::create()?,
        ],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
