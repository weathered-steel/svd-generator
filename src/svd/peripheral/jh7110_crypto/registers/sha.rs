use crate::svd::create_cluster;
use crate::Result;

pub mod csr;
pub mod klen;
pub mod rdr;
pub mod wdr;
pub mod wkr;
pub mod wlen;
pub mod wsr;

/// Creates StarFive JH7110 Crypto SHA registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "sha",
        "JH7110 Crypto SHA registers",
        0x300,
        &[
            csr::create()?,
            wdr::create()?,
            rdr::create()?,
            wsr::create()?,
            wlen::create()?,
            wkr::create()?,
            klen::create()?,
        ],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
