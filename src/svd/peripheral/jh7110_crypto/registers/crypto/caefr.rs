use crate::svd::register::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates StarFive JH7110 Crypto CAAR register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "caefr",
        "JH7110 Crypto CAEFR",
        0x30c,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "caefr",
            "Crypto CAEFR",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff)?,
            None,
        )?]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
