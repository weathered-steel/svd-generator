use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates StarFive JH7110 Crypto CA Status register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "casr",
        "JH7110 Crypto CA Status Register",
        0x4,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "done",
            "Crypto Done",
            create_bit_range("[0:0]")?,
            svd::Access::ReadOnly,
            None,
        )?]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
