use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_enum,
    create_register, create_register_properties,
};
use crate::Result;

/// Creates StarFive JH7110 Crypto AES CSR register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "csr",
        "JH7110 Crypto AES Control Status Register",
        0x24,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "cmode",
                "",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_enum(
                "keymode",
                "AES Key Mode - 0: AES 128, 1: AES 192, 2: AES 256",
                create_bit_range("[2:1]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("aes128", "AES Key Mode: 128-bit", 0)?,
                    create_enum_value("aes192", "AES Key Mode: 192-bit", 1)?,
                    create_enum_value("aes256", "AES Key Mode: 256-bit", 2)?,
                ])?],
                None,
            )?,
            create_field(
                "busy",
                "AES Engine Busy",
                create_bit_range("[3:3]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "done",
                "AES Engine Done",
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "krdy",
                "AES Key Done",
                create_bit_range("[5:5]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rst",
                "AES Reset",
                create_bit_range("[6:6]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "ie",
                "AES Interrupt Enable",
                create_bit_range("[7:7]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "ccm_start",
                "AES CCM Start",
                create_bit_range("[8:8]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_enum(
                "mode",
                "AES Mode - 0: ECB, 1: CBC, 2: CFB, 3: OFB, 4: CTR, 5: CCM, 6: GCM",
                create_bit_range("[11:9]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("ecb", "AES Mode: ECB", 0)?,
                    create_enum_value("cbc", "AES Mode: CBC", 1)?,
                    create_enum_value("cfb", "AES Mode: CFB", 2)?,
                    create_enum_value("ofb", "AES Mode: OFB", 3)?,
                    create_enum_value("ctr", "AES Mode: CTR", 4)?,
                    create_enum_value("ccm", "AES Mode: CCM", 5)?,
                    create_enum_value("gcm", "AES Mode: GCM", 6)?,
                ])?],
                None,
            )?,
            create_field(
                "gcm_start",
                "AES GCM Start",
                create_bit_range("[12:12]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "gcm_done",
                "AES GCM Done",
                create_bit_range("[13:13]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "delay_aes",
                "Delay AES",
                create_bit_range("[14:14]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "vaes_start",
                "VAES Start",
                create_bit_range("[15:15]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_enum(
                "stream_mode",
                "AES Stream Cipher mode - 0: XFB 1, 5: XFB 128",
                create_bit_range("[26:24]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("xfb1", "AES Stream Cipher Mode: XFB 1", 0)?,
                    create_enum_value("xfb128", "AES Stream Cipher Mode: XFB 128", 5)?,
                ])?],
                None,
            )?,
        ]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
