use crate::svd::register::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates StarFive JH7110 Crypto DMA registers.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "dma",
        "JH7110 Crypto DMA registers",
        0x10,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "dma_len",
            "DMA transfer length",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff)?,
            None,
        )?]),
        Some(
            svd::DimElement::builder()
                .dim(2)
                .dim_increment(0x4)
                .dim_index(Some(["_in_len", "_out_len"].map(String::from).into()))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
