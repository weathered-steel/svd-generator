use crate::svd::create_peripheral;
use crate::Result;

pub mod registers;

/// Represents a StarFive JH7110 WDT (compatible) peripheral
pub struct Jh7110Wdt {
    peripheral: svd::Peripheral,
}

impl Jh7110Wdt {
    /// Creates a new [Jh7110Wdt] peripheral.
    pub fn create(
        name: &str,
        base_address: u64,
        size: u32,
        interrupt: Option<Vec<svd::Interrupt>>,
        dim: u32,
    ) -> Result<Self> {
        let dim_element = if dim > 0 {
            Some(
                svd::DimElement::builder()
                    .dim(dim)
                    .dim_increment(size)
                    .build(svd::ValidateLevel::Strict)?,
            )
        } else {
            None
        };

        let peripheral = create_peripheral(
            name,
            format!("StarFive JH7110 WDT: {name}").as_str(),
            base_address,
            size,
            interrupt,
            Some(registers::create()?),
            dim_element,
            Some("WDT".into()),
        )?;

        Ok(Self { peripheral })
    }

    /// Gets a reference to the SVD [`Peripheral`](svd::Peripheral).
    pub const fn peripheral(&self) -> &svd::Peripheral {
        &self.peripheral
    }

    /// Gets a mutable reference to the SVD [`Peripheral`](svd::Peripheral).
    pub fn peripheral_mut(&mut self) -> &mut svd::Peripheral {
        &mut self.peripheral
    }

    /// Converts the [Jh7110Wdt] into the inner SVD [`Peripheral`](svd::Peripheral).
    pub fn to_inner(self) -> svd::Peripheral {
        self.peripheral
    }
}
