use crate::svd::register::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates StarFive JH7110 ISP SYSCON SYSCFG 8 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "isp_syscfg8",
        "ISP SYSCFG 8: isp_sysconsaif_syscfg_32",
        0x20,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "u0_vin_cfg_axiwr0_start_addr",
            "This bit represents the start address of the AXI output image.",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff)?,
            None,
        )?]),
        None,
    )?))
}
