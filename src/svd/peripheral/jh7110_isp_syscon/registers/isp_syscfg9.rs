use crate::svd::register::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_constraint,
    create_field_enum, create_register, create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates StarFive JH7110 ISP SYSCON SYSCFG 9 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "isp_syscfg9",
        "ISP SYSCFG 9: isp_sysconsaif_syscfg_36",
        0x24,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "u0_vin_cfg_color_bar_en",
                "Set this bit to 1 to use the color bar for test.",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "u0_vin_cfg_dvp_hs_pos",
                "Use DVP to AXI - 0: HS is low valid, 1: HS is high valid.",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "u0_vin_cfg_dvp_swap_en",
                "Set this bit to 1 to enable DVP swap.",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_enum(
                "u0_vin_cfg_dvp_vs_pos",
                "Use DVP to AXI - 0: VS is low valid, 1: VS is high valid.",
                create_bit_range("[3:3]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("low_valid", "VS is low valid", 0)?,
                    create_enum_value("high_valid", "VS is high valid", 1)?,
                ])?],
                None,
            )?,
            create_field(
                "u0_vin_cfg_gen_en_axird",
                "Set this bit to use AXI input for ISP and generate test image.", 
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "u0_vin_cfg_isp_dvp_en0",
                "Set this bit to use DVP input for ISP.", 
                create_bit_range("[5:5]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "u0_vin_cfg_mipi_byte_en_isp0",
                "Set to 1 for dvp_clk_inv, the DVP clock inverter register.", 
                create_bit_range("[7:6]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field_constraint(
                "u0_vin_cfg_mipi_channel_sel0",
                "Select 1 channel output of the 8 MIPI channels.", 
                create_bit_range("[11:8]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field(
                "u0_vin_cfg_mipi_header_en0",
                "Set this bit to 1 to add 10 bits to bit 2, so 1 pixel equals 12 bits.", 
                create_bit_range("[12:12]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_enum(
                "u0_vin_cfg_pix_num",
                "VIN AXI to ISP MIPI port, 12-bit data configuration - 00: axi_data[11:0], 01: axi_data[9:0], 10: axi_data[7:0], 11: axi_data[5:0]", 
                create_bit_range("[16:13]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("axi_data0_11", "12-bit data configuration: axi_data[11:0]", 0b00)?,
                    create_enum_value("axi_data0_9", "12-bit data configuration: axi_data[9:0]", 0b01)?,
                    create_enum_value("axi_data0_7", "12-bit data configuration: axi_data[7:0]", 0b10)?,
                    create_enum_value("axi_data0_5", "12-bit data configuration: axi_data[5:0]", 0b11)?,
                ])?],
                None,
            )?,
            create_field(
                "u0_vin_generic_sp",
                "This configuration is not used by JH7110.", 
                create_bit_range("[26:17]")?,
                svd::Access::ReadOnly,
                None,
            )?,
        ]),
        None,
    )?))
}
