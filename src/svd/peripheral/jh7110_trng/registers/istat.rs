use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates a StarFive JH7110 TRNG MODE register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "istat",
        "TRNG Interrupt Status Register",
        0x14,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "rand_rdy",
                "RAND Ready Status",
                create_bit_range("[0:0]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "seed_done",
                "Seed Done Status",
                create_bit_range("[1:1]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "lfsr_lockup",
                "LFSR Lockup Status",
                create_bit_range("[4:4]")?,
                svd::Access::ReadOnly,
                None,
            )?,
        ]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
