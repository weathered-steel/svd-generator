use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock AHB register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "stat",
        "TRNG STAT Register",
        0x4,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "nonce_mode",
                "TRNG Nonce operating mode",
                create_bit_range("[2:2]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "r256",
                "TRNG 256-bit random number operating mode",
                create_bit_range("[3:3]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "mission_mode",
                "TRNG Mission Mode operating mode",
                create_bit_range("[8:8]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "seeded",
                "TRNG Seeded operating mode",
                create_bit_range("[9:9]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            // There are 8 RAND fields
            // FIXME: do these LAST_RESEED fields directly correspond to the RAND fields?
            // These fields are a best-guess based on the Linux char driver
            create_field(
                "last_reseed",
                "TRNG Last Reseed status",
                create_bit_range("[16:16]")?,
                svd::Access::ReadOnly,
                Some(
                    svd::DimElement::builder()
                        .dim(8)
                        .dim_increment(1)
                        .build(svd::ValidateLevel::Strict)?,
                ),
            )?,
            create_field(
                "srvc_rqst",
                "TRNG Service Request",
                create_bit_range("[27:27]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "rand_generating",
                "TRNG Random Number Generating Status",
                create_bit_range("[30:30]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "rand_seeding",
                "TRNG Random Number Seeding Status",
                create_bit_range("[31:31]")?,
                svd::Access::ReadOnly,
                None,
            )?,
        ]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
