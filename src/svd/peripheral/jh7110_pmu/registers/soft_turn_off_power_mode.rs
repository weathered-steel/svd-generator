use crate::svd::{create_register, create_register_properties};
use crate::Result;

use super::create_fields_power_mode;

/// Creates the StarFive JH7110 PMU Software Turn-Off Power Mode register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "soft_turn_off_power_mode",
        "Software Turn-Off Power Mode",
        0x10,
        create_register_properties(32, 0)?,
        Some(&create_fields_power_mode("off")?),
        None,
    )?))
}
