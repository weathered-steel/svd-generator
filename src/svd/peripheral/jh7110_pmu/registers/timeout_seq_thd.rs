use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates the StarFive JH7110 PMU Timeout Sequence Threshold register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "timeout_seq_thd",
        "Timeout Sequence Threshold",
        0x14,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "timeout_seq_thd",
            "",
            create_bit_range("[15:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff)?,
            None,
        )?]),
        None,
    )?))
}
