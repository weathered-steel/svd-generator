use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the StarFive JH7110 PMU Hardware Event Turn-On Mask register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "hard_event_turn_on_mask",
        "Hardware Event Turn-On Mask",
        0x4,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "hard_event_rtc_on_mask",
                "RTC event encourage turn-on sequence, 0: enable hardware event, 1: mask hardware event",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "hard_event_gmac_on_mask",
                "GMAC event encourage turn-on sequence, 0: enable hardware event, 1: mask hardware event",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "hard_event_rfu_on_mask",
                "RFU, 0: enable hardware event, 1: mask hardware event",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "hard_event_rgpio0_on_mask",
                "RGPIO0 event encourage turn-on sequence, 0: enable hardware event, 1: mask hardware event",
                create_bit_range("[3:3]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "hard_event_rgpio1_on_mask",
                "RGPIO1 event encourage turn-on sequence, 0: enable hardware event, 1: mask hardware event",
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "hard_event_rgpio2_on_mask",
                "RGPIO2 event encourage turn-on sequence, 0: enable hardware event, 1: mask hardware event",
                create_bit_range("[5:5]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "hard_event_rgpio3_on_mask",
                "RGPIO3 event encourage turn-on sequence, 0: enable hardware event, 1: mask hardware event",
                create_bit_range("[6:6]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "hard_event_gpu_on_mask",
                "GPU event, 0: enable hardware event, 1: mask hardware event",
                create_bit_range("[7:7]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
