use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates Synopsys DesignWare AXI DMAC Channel Enable register.
pub fn create(dma_channels: u64) -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "chen",
        "DMAC Channel Enable register contains the DMAC channel enable settings. Only exists when DMAX_NUM_CHANNELS <= 8.",
        0x18,
        create_register_properties(64, 0)?,
        Some(&[
            create_field(
                "en_ch",
                "DMAC Channel Enable - 0: disable DMAC channel, 1: enable DMAC channel",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                Some(svd::DimElement::builder()
                    .dim(dma_channels as u32)
                    .dim_increment(1)
                    .dim_index(Some((1..=dma_channels).map(|s| format!("{s}")).collect::<Vec<String>>()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
            create_field(
                "en_we_ch",
                "DMAC Channel Enable Write-enable - 0: disable write to DMAC channel enable bit, 1: enable write to DMAC channel enable bit",
                create_bit_range("[8:8]")?,
                svd::Access::WriteOnly,
                Some(svd::DimElement::builder()
                    .dim(dma_channels as u32)
                    .dim_increment(1)
                    .dim_index(Some((1..=dma_channels).map(|s| format!("{s}")).collect::<Vec<String>>()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
            create_field(
                "susp_ch",
                "DMAC Channel Suspend - 0: no DMAC channel suspend request, 1: DMAC channel suspend request",
                create_bit_range("[16:16]")?,
                svd::Access::ReadWrite,
                Some(svd::DimElement::builder()
                    .dim(dma_channels as u32)
                    .dim_increment(1)
                    .dim_index(Some((1..=dma_channels).map(|s| format!("{s}")).collect::<Vec<String>>()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
            create_field(
                "susp_we_ch",
                "DMAC Channel Suspend Write-enable - 0: disable write to DMAC channel suspend bit, 1: enable write to DMAC channel suspend bit",
                create_bit_range("[24:24]")?,
                svd::Access::WriteOnly,
                Some(svd::DimElement::builder()
                    .dim(dma_channels as u32)
                    .dim_increment(1)
                    .dim_index(Some((1..=dma_channels).map(|s| format!("{s}")).collect::<Vec<String>>()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
            create_field(
                "abort_ch",
                "DMAC Channel Abort - 0: no DMAC channel abort request, 1: request DMAC channel abort. Memory access depends on DMAX_CH_ABORT_EN configuration setting - 0: read-only, 1: read-write",
                create_bit_range("[32:32]")?,
                // FIXME: use build configuration to allow user to set DMAX_CH_ABORT_EN
                // setting?
                svd::Access::ReadWrite,
                Some(svd::DimElement::builder()
                    .dim(dma_channels as u32)
                    .dim_increment(1)
                    .dim_index(Some((1..=dma_channels).map(|s| format!("{s}")).collect::<Vec<String>>()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
            create_field(
                "abort_we_ch",
                "DMAC Channel Abort Write-enable - 0: disable write to DMAC channel abort bit, 1: enable write DMAC channel abort. Memory access depends on DMAX_CH_ABORT_EN configuration setting - 0: read-only, 1: write-only",
                create_bit_range("[40:40]")?,
                // FIXME: use build configuration to allow user to set DMAX_CH_ABORT_EN
                // setting?
                svd::Access::ReadWrite,
                Some(svd::DimElement::builder()
                    .dim(dma_channels as u32)
                    .dim_increment(1)
                    .dim_index(Some((1..=dma_channels).map(|s| format!("{s}")).collect::<Vec<String>>()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
        ]),
        None,
    )?))
}
