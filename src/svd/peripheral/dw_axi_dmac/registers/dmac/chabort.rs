use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates Synopsys DesignWare AXI DMAC Channel Abort register.
pub fn create(dma_channels: u64) -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "chabort",
        "DMAC Channel Abort register contains the DMAC channel abort settings. Only exists when DMAX_NUM_CHANNELS > 8",
        0x28,
        create_register_properties(64, 0)?,
        create_fields(dma_channels)?.as_deref(),
        None,
    )?))
}

fn create_fields(dma_channels: u64) -> Result<Option<Vec<svd::Field>>> {
    let abort1_field_len = dma_channels.saturating_sub(16) as usize;

    match dma_channels {
        ch if ch > 16 => Ok(Some(create_abort0_fields(dma_channels)?.into_iter().chain([
            create_field(
                "abort1_ch",
                "DMAC Channel Abort - 0: no DMAC channel abort request, 1: request DMAC channel abort. Memory access depends on DMAX_CH_ABORT_EN configuration setting - 0: read-only, 1: read-write",
                create_bit_range("[32:32]")?,
                // FIXME: use build configuration to allow user to set DMAX_CH_ABORT_EN
                // setting?
                svd::Access::ReadWrite,
                Some(svd::DimElement::builder()
                    .dim(abort1_field_len as u32)
                    .dim_increment(1)
                    .dim_index(Some((17..=32).map(|s| format!("{s}")).take(abort1_field_len).collect::<Vec<String>>()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
            create_field(
                "abort_we1_ch",
                "DMAC Channel Abort Write-enable - 0: disable write to DMAC channel abort bit, 1: enable write DMAC channel abort. Memory access depends on DMAX_CH_ABORT_EN configuration setting - 0: read-only, 1: write-only",
                create_bit_range("[48:48]")?,
                // FIXME: use build configuration to allow user to set DMAX_CH_ABORT_EN
                // setting?
                svd::Access::ReadWrite,
                Some(svd::DimElement::builder()
                    .dim(abort1_field_len as u32)
                    .dim_increment(1)
                    .dim_index(Some((17..=32).map(|s| format!("{s}")).take(abort1_field_len).collect::<Vec<String>>()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
        ]).collect())),
        ch if ch > 8 => Ok(Some(create_abort0_fields(dma_channels)?)),
        _ => Ok(None),
    }
}

fn create_abort0_fields(dma_channels: u64) -> Result<Vec<svd::Field>> {
    let (abort_ch, abort_we_ch, abort_len) = match dma_channels {
        ch if ch > 16 => ("abort0_ch", "abort_we0_ch", 16),
        ch if ch > 8 => ("abort_ch", "abort_we_ch", ch as usize),
        _ => ("", "", 0),
    };

    match abort_len {
        0 => Ok(vec![]),
        _ => Ok(vec![
            create_field(
                abort_ch,
                "DMAC Channel Abort - 0: no DMAC channel abort request, 1: request DMAC channel abort. Memory access depends on DMAX_CH_ABORT_EN configuration setting - 0: read-only, 1: read-write",
                create_bit_range("[0:0]")?,
                // FIXME: use build configuration to allow user to set DMAX_CH_ABORT_EN
                // setting?
                svd::Access::ReadWrite,
                Some(svd::DimElement::builder()
                    .dim(abort_len as u32)
                    .dim_increment(1)
                    .dim_index(Some((1..=16).map(|s| format!("{s}")).take(abort_len).collect::<Vec<String>>()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
            create_field(
                abort_we_ch,
                "DMAC Channel Abort Write-enable - 0: disable write to DMAC channel abort bit, 1: enable write DMAC channel abort. Memory access depends on DMAX_CH_ABORT_EN configuration setting - 0: read-only, 1: write-only",
                create_bit_range("[16:16]")?,
                // FIXME: use build configuration to allow user to set DMAX_CH_ABORT_EN
                // setting?
                svd::Access::ReadWrite,
                Some(svd::DimElement::builder()
                    .dim(abort_len as u32)
                    .dim_increment(1)
                    .dim_index(Some((1..=16).map(|s| format!("{s}")).take(abort_len).collect::<Vec<String>>()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
        ]),
    }
}
