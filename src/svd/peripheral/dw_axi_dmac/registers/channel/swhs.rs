use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates Synopsys DesignWare AXI DMAC Channel Software Handshake register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "swhs",
        "Software Handshake register.",
        0x38,
        create_register_properties(64, 0)?,
        Some(&[
            create_field(
                "req_src",
                "Software Handshake Request signal configuration: register source.",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "req_src_we",
                "Software Handshake Request signal configuration: register source write-enable.",
                create_bit_range("[1:1]")?,
                svd::Access::WriteOnly,
                None,
            )?,
            create_field(
                "req_sgl_src",
                "Software Handshake Request signal configuration: register scatter-gather source.",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "req_sgl_src_we",
                "Software Handshake Request signal configuration: register scatter-gather source write-enable.",
                create_bit_range("[3:3]")?,
                svd::Access::WriteOnly,
                None,
            )?,
            create_field(
                "req_lst_src",
                "Software Handshake Request signal configuration: register list source.",
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "req_lst_src_we",
                "Software Handshake Request signal configuration: register list source write-enable.",
                create_bit_range("[5:5]")?,
                svd::Access::WriteOnly,
                None,
            )?,
        ]),
        Some(
            svd::DimElement::builder()
                .dim(2)
                .dim_increment(0x8)
                .dim_index(Some([String::from("_src"), String::from("_dst")].into()))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
