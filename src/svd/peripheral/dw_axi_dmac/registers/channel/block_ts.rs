use crate::svd::register::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates Synopsys DesignWare AXI DMAC Block Transfer Size register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "block_ts",
        "DMAC Block transfer size.",
        0x10,
        create_register_properties(64, 0)?,
        Some(&[create_field_constraint(
            "block_ts",
            "Block transfer size of DMA transfer",
            create_bit_range("[21:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0x3f_ffff)?,
            None,
        )?]),
        None,
    )?))
}
