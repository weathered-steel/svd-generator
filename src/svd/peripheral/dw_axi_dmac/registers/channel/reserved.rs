use crate::svd::register::{create_register, create_register_properties};
use crate::Result;

/// Creates Synopsys DesignWare AXI DMAC Channel Reserved registers.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "_reserved_channel",
        "DMAC Reserved register.",
        0xf8,
        create_register_properties(64, 0)?,
        None,
        None,
    )?))
}
