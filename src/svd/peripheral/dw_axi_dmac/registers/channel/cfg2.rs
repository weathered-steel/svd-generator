use crate::svd::register::{
    create_bit_range, create_enum_value, create_enum_values, create_field_constraint,
    create_field_enum, create_register, create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates Synopsys DesignWare AXI DMAC Channel Configuration register.
pub fn create(dma_channels: u64) -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "cfg2",
        "DMAC Channel Configuration 2 register (only exists for DMAX_NUM_CHANNELS > 8).",
        0x20,
        create_register_properties(64, 0)?,
        Some(&[
            create_field_enum(
                "multblk_type",
                "Source Multi Block Transfer Type - 0b00: Contiguous, 0b01: Reload, 0b10: Shadow Register, 0b11: Linked List",
                create_bit_range("[1:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("contiguous", "Contiguous multi-block transfer type", 0b00)?,
                    create_enum_value("reload", "Reload multi-block transfer type", 0b01)?,
                    create_enum_value("shadow", "Shadow register multi-block transfer type", 0b10)?,
                    create_enum_value("linked_list", "Linked-list multi-block transfer type", 0b11)?,
                ])?],
                Some(svd::DimElement::builder()
                    .dim(2)
                    .dim_increment(2)
                    .dim_index(Some([
                        String::from("_src"),
                        String::from("_dst"),
                    ].into()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
            create_field_constraint(
                "per",
                "Assigns a hardware handshaking interface - **NOTE** only one peripheral should be assigned to the same interface.",
                create_bit_range("[10:4]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7f)?,
                Some(svd::DimElement::builder()
                    .dim(2)
                    .dim_increment(7)
                    .dim_index(Some([
                        String::from("_src"),
                        String::from("_dst"),
                    ].into()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
            create_field_enum(
                "tt_fc",
                "Transfer Type and Flow Control - 0: tt=mem-to-mem, fc=dw_axi_dmac, 1: tt=mem-to-per, fc=dw_axi_dmac, 2: tt=per-to-mem, fc=dw_axi_dmac, 3: tt=per-to-per, fc=dw_axi_dmac, 4: tt=per-to-mem, fc=source-peripheral, 5: tt=per-to-per, fc=source-peripheral, 6: tt=mem-to-per, fc=destination-peripheral, 7: tt=per-to-per, fc=destination-peripheral",
                create_bit_range("[34:32]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("tt_mtm_fc_dmac", "Transfer type: mem-to-mem, Flow Control: dw_axi_dmac", 0)?,
                    create_enum_value("tt_mtp_fc_dmac", "Transfer type: mem-to-per, Flow Control: dw_axi_dmac", 1)?,
                    create_enum_value("tt_ptm_fc_dmac", "Transfer type: per-to-mem, Flow Control: dw_axi_dmac", 2)?,
                    create_enum_value("tt_ptp_fc_dmac", "Transfer type: per-to-per, Flow Control: dw_axi_dmac", 3)?,
                    create_enum_value("tt_ptm_fc_srcp", "Transfer type: per-to-mem, Flow Control: source-peripheral", 4)?,
                    create_enum_value("tt_ptp_fc_srcp", "Transfer type: per-to-per, Flow Control: source-peripheral", 5)?,
                    create_enum_value("tt_mtp_fc_dstp", "Transfer type: mem-to-per, Flow Control: destination-peripheral", 6)?,
                    create_enum_value("tt_ptp_fc_dstp", "Transfer type: per-to-per, Flow Control: destination-peripheral", 7)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "hs_sel",
                "Source Software or Hardware Handshaking Select - 0: hardware handshake, 1: software handshake",
                create_bit_range("[35:35]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("hardware", "Hardware handshake", 0)?,
                    create_enum_value("software", "Software handshake", 1)?,
                ])?],
                Some(svd::DimElement::builder()
                    .dim(2)
                    .dim_increment(1)
                    .dim_index(Some([
                        String::from("_src"),
                        String::from("_dst"),
                    ].into()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
            create_field_enum(
                "hwhs_pol",
                "Hardware Handshaking Polarity - 0: active high, 1: active low",
                create_bit_range("[37:37]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("active_high", "Hardware handshaking polarity active high", 0)?,
                    create_enum_value("active_low", "Hardware handshaking polarity active low", 1)?,
                ])?],
                Some(svd::DimElement::builder()
                    .dim(2)
                    .dim_increment(1)
                    .dim_index(Some([
                        String::from("_src"),
                        String::from("_dst"),
                    ].into()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
            create_field_constraint(
                "ch_prior",
                "Channel priority - 0: lowest, NUM_CHAN - 1: highest. **NOTE** a value outside this range leads to undefined behavior.",
                create_bit_range("[51:47]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, dma_channels.saturating_sub(1))?,
                None,
            )?,
            create_field_enum(
                "lock_ch",
                "Lock Channel - 0: no lock, 1: lock.",
                create_bit_range("[52:52]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("no_lock", "No channel lock", 0)?,
                    create_enum_value("lock", "Channel lock", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "ch_lock_lvl",
                "Channel Lock Level - 0: entire transfer, 1: current block.",
                create_bit_range("[53:53]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("entire_transfer", "Channel lock for the entire transfer", 0)?,
                    create_enum_value("current_block", "Channel lock for the current block", 1)?,
                ])?],
                None,
            )?,
            create_field_constraint(
                "osr_lmt",
                "Outstanding Request Limit. **NOTE** Maximum outstanding request limit is 16.",
                create_bit_range("[58:55]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                Some(svd::DimElement::builder()
                    .dim(2)
                    .dim_increment(4)
                    .dim_index(Some([
                        String::from("_src"),
                        String::from("_dst"),
                    ].into()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
        ]),
        None,
    )?))
}
