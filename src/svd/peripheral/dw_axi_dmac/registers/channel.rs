use crate::svd::register::create_cluster;
use crate::Result;

pub mod axi_id;
pub mod axi_qos;
pub mod blk_tr_resume_req;
pub mod block_ts;
pub mod cfg;
pub mod cfg2;
pub mod ctl;
pub mod dar;
pub mod int;
pub mod llp;
pub mod reserved;
pub mod sar;
pub mod stat;
pub mod statar;
pub mod status;
pub mod swhs;

/// Creates Synopsys DesignWare AXI DMAC Channel registers.
pub fn create(dma_channels: u64) -> Result<svd::RegisterCluster> {
    let cfg = match dma_channels {
        ch if ch <= 8 => cfg::create(ch)?,
        ch => cfg2::create(ch)?,
    };
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "ch",
        "DesignWare DMAC Channel registers",
        0x100,
        &[
            sar::create()?,
            dar::create()?,
            block_ts::create()?,
            ctl::create()?,
            cfg,
            llp::create()?,
            status::create()?,
            swhs::create()?,
            blk_tr_resume_req::create()?,
            axi_id::create()?,
            axi_qos::create()?,
            stat::create()?,
            statar::create()?,
            int::create()?,
            reserved::create()?,
        ],
        Some(
            svd::DimElement::builder()
                .dim(dma_channels as u32)
                .dim_increment(0x100)
                .dim_index(Some(
                    (1..=dma_channels)
                        .map(|ch| format!("{ch}"))
                        .collect::<Vec<String>>(),
                ))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
