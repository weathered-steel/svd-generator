use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates Synopsys DesignWare Gigabit Ethernet v4.xx Hardware Features 2 register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "features2",
        "Hardware Features 2",
        0x8,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "rxqcnt",
                "RX Queue Count",
                create_bit_range("[3:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field_constraint(
                "txqcnt",
                "TX Queue Count",
                create_bit_range("[9:6]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field_constraint(
                "rxchcnt",
                "RX Channel Count",
                create_bit_range("[15:12]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field_constraint(
                "txchcnt",
                "TX Channel Count",
                create_bit_range("[21:18]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field_constraint(
                "ppsoutnum",
                "PPS Out Number",
                create_bit_range("[26:24]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7)?,
                None,
            )?,
            create_field_constraint(
                "auxsnapnum",
                "AUX Snap Number",
                create_bit_range("[30:28]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7)?,
                None,
            )?,
        ]),
        None,
    )?))
}
