use crate::svd::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates Synopsys DesignWare Gigabit Ethernet v4.xx Hardware Features 3 register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "features3",
        "Hardware Features 3",
        0xc,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "nrvf",
                "NRVF",
                create_bit_range("[2:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7)?,
                None,
            )?,
            create_field(
                "dvlan",
                "DVLAN",
                create_bit_range("[5:5]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "frpsel",
                "FRP Select",
                create_bit_range("[10:10]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "frpbs",
                "FRP BS",
                create_bit_range("[12:11]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field_constraint(
                "frpes",
                "FRP ES",
                create_bit_range("[14:13]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field(
                "estsel",
                "EST Select",
                create_bit_range("[16:16]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "estdep",
                "EST DEP",
                create_bit_range("[19:17]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7)?,
                None,
            )?,
            create_field_constraint(
                "estwid",
                "EST WID",
                create_bit_range("[21:20]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field(
                "fpesel",
                "FPE Select",
                create_bit_range("[26:26]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "tbssel",
                "TBS Select",
                create_bit_range("[27:27]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "asp",
                "ASP",
                create_bit_range("[29:28]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
        ]),
        None,
    )?))
}
