use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates a Synopsys DesignWare Gigabit Ethernet MAC v4.xx GPIO Status register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "gpio_status",
        "MAC GPIO Status",
        0x20c,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "gpo",
            "MAC GPIO GPO Status",
            create_bit_range("[16:16]")?,
            svd::Access::ReadWrite,
            Some(
                svd::DimElement::builder()
                    .dim(4)
                    .dim_increment(0x1)
                    .build(svd::ValidateLevel::Strict)?,
            ),
        )?]),
        None,
    )?))
}
