use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Synopsys DesignWare Gigabit Ethernet v4.xx MTL Interrupt Status register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "int_status",
        "MTL Interrupt Status",
        0x20,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "queue",
            "MTL Interrupt Queue",
            create_bit_range("[0:0]")?,
            svd::Access::ReadWrite,
            Some(
                svd::DimElement::builder()
                    .dim(32)
                    .dim_increment(0x1)
                    .build(svd::ValidateLevel::Strict)?,
            ),
        )?]),
        None,
    )?))
}
