use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates Synopsys DesignWare Gigabit Ethernet v4.xx MTL RX Queue DMA register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "rx_queue_dma",
        "MTL RX Queue DMA - rx_queue_dma0: channel 0-3, rx_queue_dma1: channel 4-7",
        0x30,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "channel",
            "RX DMA Channel",
            create_bit_range("[3:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xf)?,
            Some(
                svd::DimElement::builder()
                    .dim(4)
                    .dim_increment(0x8)
                    .build(svd::ValidateLevel::Strict)?,
            ),
        )?]),
        Some(
            svd::DimElement::builder()
                .dim(2)
                .dim_increment(0x4)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
