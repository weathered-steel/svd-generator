use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Synopsys DesignWare Gigabit Ethernet v4.xx L3/L4 Filter Control register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "l3l4_ctrl",
        "L3/L4 Filter Control",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "l3pen",
                "L3 Filter PEN",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "l3sam",
                "L3 Filter SAM",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "l3saim",
                "L3 Filter SAIM",
                create_bit_range("[3:3]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "l3dam",
                "L3 Filter DAM",
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "l3daim",
                "L3 Filter DAIM",
                create_bit_range("[5:5]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "l4pen",
                "L4 Filter PEN",
                create_bit_range("[16:16]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "l4spm",
                "L4 Filter SPM",
                create_bit_range("[18:18]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "l4spim",
                "L4 Filter SPIM",
                create_bit_range("[19:19]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "l4dpm",
                "L4 Filter DPM",
                create_bit_range("[20:20]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "l4dpim",
                "L4 Filter DPIM",
                create_bit_range("[21:21]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
