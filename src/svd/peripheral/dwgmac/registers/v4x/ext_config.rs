use crate::svd::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates a Synopsys DesignWare Gigabit Ethernet MAC v4.xx Extended Configuration register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "ext_config",
        "MAC Extended Configuration",
        0x4,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "hdsms",
                "HDSMS",
                create_bit_range("[22:20]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7)?,
                None,
            )?,
            create_field(
                "eipg_en",
                "EIPG Enable",
                create_bit_range("[24:24]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "eipg",
                "EIPG",
                create_bit_range("[29:25]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x1f)?,
                None,
            )?,
        ]),
        None,
    )?))
}
