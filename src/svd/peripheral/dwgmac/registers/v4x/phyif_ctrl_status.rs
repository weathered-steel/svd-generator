use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_enum,
    create_register, create_register_properties,
};
use crate::Result;

/// Creates a Synopsys DesignWare Gigabit Ethernet MAC v4.xx PHY Interface Control and Status register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "phyif_ctrl_status",
        "PHY Interface Control and Status",
        0xf8,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "tc",
                "PHY TC",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "lud",
                "PHY LUD",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "smidrxs",
                "PHY SMID RXS",
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "lnkmod",
                "PHY Link Mode",
                create_bit_range("[16:16]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_enum(
                "speed",
                "PHY Link Speed",
                create_bit_range("[18:17]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("speed2_5", "PHY Link Speed 2.5", 0b00)?,
                    create_enum_value("speed25", "PHY Link Speed 25", 0b01)?,
                    create_enum_value("speed125", "PHY Link Speed 125", 0b10)?,
                ])?],
                None,
            )?,
            create_field(
                "lnksts",
                "PHY Link Status",
                create_bit_range("[19:19]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "jabto",
                "PHY Jabber TO",
                create_bit_range("[20:20]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "falsecardet",
                "PHY False CARDET",
                create_bit_range("[21:21]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
