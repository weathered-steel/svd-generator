use crate::svd::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates a Synopsys DesignWare Gigabit Ethernet MAC v4.xx TX Queue Flow Control register
/// definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "tx_queue_flow_ctrl",
        "MAC TX Queue Flow Control",
        0x70,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "tfe",
                "Tranmission Flow Enable",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "pt",
                "Pause Time",
                create_bit_range("[31:16]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xffff)?,
                None,
            )?,
        ]),
        Some(
            svd::DimElement::builder()
                .dim(8)
                .dim_increment(0x4)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
