use super::{pcs, pmt, v4x::*};
use crate::Result;

pub mod dma;
pub mod mtl;
pub mod pps;
pub mod pps_ctrl;
pub mod vff_queue_ctrl;

/// Creates Synopsys DesignWare Gigabit Ethernet MAC v5.xx register definitions.
///
/// Based on the register definitions from the Linux driver: <https://elixir.bootlin.com/linux/latest/source/drivers/net/ethernet/stmicro/stmmac/dwmac5.h>
///
/// Original Author: Jose Abreu <joabreu@synopsys.com>
pub fn create() -> Result<Vec<svd::RegisterCluster>> {
    Ok([
        config::create()?,
        ext_config::create()?,
        packet_filter::create()?,
        hash_table::create()?,
        vlan_tag::create()?,
        vlan_tag_data::create()?,
        vlan_hash_table::create()?,
        vlan::create()?,
        tx_queue_flow_ctrl::create()?,
        rx_flow_ctrl::create()?,
        vff_queue_ctrl::create()?,
        tx_queue_priority::create()?,
        rx_queue_ctrl::create()?,
        int::create()?,
        pmt::create(0xc0)?,
        lpi::create()?,
        tic_counter_us::create()?,
        pcs::create(0xe0)?,
        phyif_ctrl_status::create()?,
        debug::create()?,
        hw_feat::create()?,
        mdio::create()?,
        gpio_status::create()?,
        arp_addr::create()?,
        addr::create()?,
        l3l4::create()?,
        timestamp::create()?,
        pps_ctrl::create()?,
        pps::create()?,
        mtl::create()?,
        dma::create()?,
    ]
    .into())
}
