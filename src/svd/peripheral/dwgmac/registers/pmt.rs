use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates the Synopsys DesignWare Gigabit Ethernet MAC PMT register definition.
pub fn create(offset: u32) -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "pmt",
        "PMT Control and Status",
        offset,
        create_register_properties(32, 0)?,
        Some(&[create_field_enum(
            "power_event",
            "PMT Power Event",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            &[create_enum_values(&[
                create_enum_value("pointer_reset", "Pointer reset", 0x8000_0000)?,
                create_enum_value("global_unicast", "Global Unicast", 0x0000_0200)?,
                create_enum_value("wake_up_rx_frame", "Wake-up Receive Frame", 0x0000_0040)?,
                create_enum_value("magic_frame", "Magic Frame", 0x0000_0020)?,
                create_enum_value("wake_up_frame_en", "Wake-up Frame Enable", 0x0000_0004)?,
                create_enum_value("magic_pkt_en", "Magic Packet Enable", 0x0000_0002)?,
                create_enum_value("power_down", "Power Down", 0x0000_0001)?,
            ])?],
            None,
        )?]),
        None,
    )?))
}
