use crate::svd::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates a Synopsys DesignWare Gigabit Ethernet MAC v5.xx EQoS VLAN Tag Filter Fail Packets Queuing register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "vff_queue_ctrl",
        "MAC EQoS VLAN Tag Filter Fail Packets Queuing",
        0x94,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "qe",
                "VLAN Tag Filter Fail Queue Enable",
                create_bit_range("[16:16]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "vffq",
                "VLAN Tag Filter Fail Queue",
                create_bit_range("[19:17]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7)?,
                None,
            )?,
        ]),
        None,
    )?))
}
