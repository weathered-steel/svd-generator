use crate::svd::create_cluster;
use crate::svd::peripheral::dwgmac::registers::v4x::mtl::*;
use crate::Result;

pub mod dpp_ctrl;
pub mod ecc_ctrl;
pub mod ecc_int;
pub mod rxp;
pub mod safety_int_status;

/// Creates Synopsys DesignWare Gigabit Ethernet v5.xx MTL register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "mtl",
        "MTL registers",
        0xc00,
        &[
            operation_mode::create()?,
            int_status::create()?,
            rx_queue_dma::create()?,
            rxp::create()?,
            ecc_ctrl::create()?,
            safety_int_status::create()?,
            ecc_int::create()?,
            dpp_ctrl::create()?,
            chan::create()?,
        ],
        None,
    )?))
}
