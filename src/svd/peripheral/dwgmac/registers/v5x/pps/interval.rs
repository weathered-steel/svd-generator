use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates Synopsys DesignWare Gigabit Ethernet v5.xx PPS Interval register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "interval",
        "PPS Interval",
        0x8,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "interval",
            "PPS Interval",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff)?,
            None,
        )?]),
        None,
    )?))
}
