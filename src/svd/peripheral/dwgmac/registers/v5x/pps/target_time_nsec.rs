use crate::svd::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates Synopsys DesignWare Gigabit Ethernet v5.xx PPS Target Time Nanoseconds register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "target_time_nsec",
        "PPS Target Time - Nanoseconds",
        0x4,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "time",
                "Target Time - Nanoseconds",
                create_bit_range("[30:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7fff_ffff)?,
                None,
            )?,
            create_field(
                "busy",
                "Target Busy",
                create_bit_range("[31:31]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
