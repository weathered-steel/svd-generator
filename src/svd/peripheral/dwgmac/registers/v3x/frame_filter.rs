use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the Synopsys DesignWare Gigabit Ethernet MAC Frame Filter register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "frame_filter",
        "Frame Filter",
        0x4,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "pr",
                "Promiscuous Mode",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "huc",
                "Hash Unicast",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "hmc",
                "Hash Multicast",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "daif",
                "DA Inverse Filtering",
                create_bit_range("[3:3]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "pm",
                "Pass All Multicast",
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "dbf",
                "Disable Broadcast Frames",
                create_bit_range("[5:5]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "pcf",
                "Pass Control Frames",
                create_bit_range("[7:7]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "saif",
                "Inverse Filtering",
                create_bit_range("[8:8]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "saf",
                "Source Address Filter",
                create_bit_range("[9:9]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "hpf",
                "Hash or Perfect Filter",
                create_bit_range("[10:10]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "ra",
                "Receive All Mode",
                create_bit_range("[31:31]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
