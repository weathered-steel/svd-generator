use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates the Synopsys DesignWare Gigabit Ethernet MAC Multicast Hash Table High register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "hash",
        "Multicast Hash Table - 64-bit big-endian hash table split across two registers",
        0x8,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "hash",
            "Multicast Hash Table",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff)?,
            None,
        )?]),
        Some(
            svd::DimElement::builder()
                .dim(2)
                .dim_increment(0x4)
                .dim_index(Some([String::from("_high"), String::from("_low")].into()))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
