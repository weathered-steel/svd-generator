use crate::svd::{
    create_bit_range, create_cluster, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates the Synopsys DesignWare Gigabit Ethernet MAC Energy Efficient Ethernet (EEE) LPI register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "lpi",
        "Energy Efficient Ethernet (EEE) LPI registers",
        0x30,
        &[
            svd::RegisterCluster::Register(create_register(
                "ctrl_status",
                "LPI Control Status",
                0x0,
                create_register_properties(32, 0)?,
                Some(&[
                    create_field(
                        "tlpien",
                        "Transmit LPI Entry",
                        create_bit_range("[0:0]")?,
                        svd::Access::ReadWrite,
                        None,
                    )?,
                    create_field(
                        "tlpiex",
                        "Transmit LPI Exit",
                        create_bit_range("[1:1]")?,
                        svd::Access::ReadWrite,
                        None,
                    )?,
                    create_field(
                        "rlpien",
                        "Receive LPI Entry",
                        create_bit_range("[2:2]")?,
                        svd::Access::ReadWrite,
                        None,
                    )?,
                    create_field(
                        "rlpiex",
                        "Receive LPI Exit",
                        create_bit_range("[3:3]")?,
                        svd::Access::ReadWrite,
                        None,
                    )?,
                    create_field(
                        "tlpist",
                        "Transmit LPI State",
                        create_bit_range("[8:8]")?,
                        svd::Access::ReadWrite,
                        None,
                    )?,
                    create_field(
                        "rlpist",
                        "Receive LPI State",
                        create_bit_range("[9:9]")?,
                        svd::Access::ReadWrite,
                        None,
                    )?,
                    create_field(
                        "lpien",
                        "LPI Enable",
                        create_bit_range("[16:16]")?,
                        svd::Access::ReadWrite,
                        None,
                    )?,
                    create_field(
                        "pls",
                        "PHY Link Status",
                        create_bit_range("[17:17]")?,
                        svd::Access::ReadWrite,
                        None,
                    )?,
                    create_field(
                        "plsen",
                        "Enable PHY Link Status",
                        create_bit_range("[18:18]")?,
                        svd::Access::ReadWrite,
                        None,
                    )?,
                    create_field(
                        "lpitxa",
                        "Enable LPI TX Automate",
                        create_bit_range("[19:19]")?,
                        svd::Access::ReadWrite,
                        None,
                    )?,
                ]),
                None,
            )?),
            svd::RegisterCluster::Register(create_register(
                "timer_ctrl",
                "LPI Timer and Control",
                0x4,
                create_register_properties(32, 0)?,
                Some(&[create_field_constraint(
                    "timer_ctrl",
                    "LPI Timer and Control",
                    create_bit_range("[31:0]")?,
                    svd::Access::ReadWrite,
                    create_write_constraint(0, 0xffff_ffff)?,
                    None,
                )?]),
                None,
            )?),
        ],
        None,
    )?))
}
