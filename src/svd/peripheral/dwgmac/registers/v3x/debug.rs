use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_constraint,
    create_field_enum, create_register, create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates the Synopsys DesignWare Gigabit Ethernet MAC Debug register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "debug",
        "Debug",
        0x24,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "rpests",
                "MAC GMII or MII Receive Protocol Engine Status",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "rfcfcsts",
                "MAC Receive Frame Controller FIFO Status",
                create_bit_range("[2:1]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field(
                "rwcsts",
                "MTL RX FIFO Write Controller Active",
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_enum(
                "rrcsts",
                "MTL RX FIFO Read Controller - 0: IDLE, 1: RDATA, 2: RSTAT, 3: FLUSH",
                create_bit_range("[6:5]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("idle", "MTL RX FIFO Read Controller has idle status", 0x0)?,
                    create_enum_value(
                        "rdata",
                        "MTL RX FIFO Read Controller has rdata status",
                        0x1,
                    )?,
                    create_enum_value(
                        "rstat",
                        "MTL RX FIFO Read Controller has rstat status",
                        0x2,
                    )?,
                    create_enum_value(
                        "flush",
                        "MTL RX FIFO Read Controller has flush status",
                        0x3,
                    )?,
                ])?],
                None,
            )?,
            create_field_enum(
                "rxfsts",
                "MTL RX FIFO Fill-level - 0: EMPTY, 1: BT, 2: AT, 3: FULL",
                create_bit_range("[9:8]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("empty", "MTL RX FIFO fill level is empty", 0x0)?,
                    create_enum_value("bt", "MTL RX FIFO fill level is bt", 0x1)?,
                    create_enum_value("at", "MTL RX FIFO fill level is at", 0x2)?,
                    create_enum_value("full", "MTL RX FIFO fill level is full", 0x3)?,
                ])?],
                None,
            )?,
            create_field(
                "tpests",
                "MAC GMII or MII Transmit Protocol Engine Status",
                create_bit_range("[16:16]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_enum(
                "tfcsts",
                "MAC Transmit Frame Controller Status - 0: IDLE, 1: WAIT, 2: GEN_PAUSE, 3: XFER",
                create_bit_range("[18:17]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("idle", "MTL Transmit Frame Controller status is IDLE", 0x0)?,
                    create_enum_value("wait", "MTL Transmit Frame Controller status is WAIT", 0x1)?,
                    create_enum_value(
                        "gen_pause",
                        "MTL Transmit Frame Controller status is GEN_PAUSE",
                        0x2,
                    )?,
                    create_enum_value("xfer", "MTL Transmit Frame Controller status is XFER", 0x3)?,
                ])?],
                None,
            )?,
            create_field(
                "txpaused",
                "MAC Transmitter in PAUSE",
                create_bit_range("[19:19]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_enum(
                "trcsts",
                "MTL Tx FIFO Read Controller Status - 0: IDLE, 1: READ, 2: TXW, 3: WRITE",
                create_bit_range("[21:20]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("idle", "MTL TX FIFO Read Controller status is IDLE", 0x0)?,
                    create_enum_value("read", "MTL TX FIFO Read Controller status is READ", 0x1)?,
                    create_enum_value("txw", "MTL TX FIFO Read Controller status is TXW", 0x2)?,
                    create_enum_value("write", "MTL TX FIFO Read Controller status is WRITE", 0x3)?,
                ])?],
                None,
            )?,
            create_field(
                "twcsts",
                "MTL TX FIFO Write Controller",
                create_bit_range("[22:22]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "txfsts",
                "MTL TX FIFO Not Empty Status",
                create_bit_range("[24:24]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "txstsfsts",
                "MTL TX Status FIFO Full Status",
                create_bit_range("[25:25]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
