use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the Synopsys DesignWare Gigabit Ethernet MAC Interrupt Status register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "status",
        "Interrupt Status",
        // Offset inside cluster, absolute offset: 0x38
        0x0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "pmt",
                "PMT Interrupt Status",
                create_bit_range("[3:3]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "mmcis",
                "MMC IS Interrupt Status",
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "mmcris",
                "MMC RIS Interrupt Status",
                create_bit_range("[5:5]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "mmctis",
                "MMC TIS Interrupt Status",
                create_bit_range("[6:6]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "mmccsum",
                "MMC Checksum Interrupt Status",
                create_bit_range("[7:7]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "tstamp",
                "Timestamp Interrupt Status",
                create_bit_range("[9:9]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "lpiis",
                "LPI IS Interrupt Status",
                create_bit_range("[10:10]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
