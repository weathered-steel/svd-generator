use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_enum,
    create_register, create_register_properties,
};
use crate::Result;

/// Creates the Synopsys DesignWare Gigabit Ethernet MAC SGMII/RGMII Status register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "rgsmiiis",
        "SGMII/RGMII Status",
        0xd8,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "lnkmode",
                "Link Mode",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_enum(
                "speed",
                "Link Speed",
                create_bit_range("[2:1]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("speed2_5", "Link Speed 2.5", 0x0)?,
                    create_enum_value("speed25", "Link Speed 25", 0x1)?,
                    create_enum_value("speed125", "Link Speed 125", 0x2)?,
                ])?],
                None,
            )?,
            create_field(
                "lnksts",
                "Link Status",
                create_bit_range("[3:3]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "jabto",
                "Jabber TO",
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "falsecardet",
                "False CARDET",
                create_bit_range("[5:5]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "smidrxs",
                "SMIDRXS",
                create_bit_range("[16:16]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
