use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates the Synopsys DesignWare Gigabit Ethernet MAC PCS Auto-Negotiation TBI Extend register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "tbi",
        "TBI Extend Status",
        0x14,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "tbi",
            "TBI Extend Status",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff)?,
            None,
        )?]),
        None,
    )?))
}
