use super::{pcs, pmt};
use crate::Result;

pub mod addr;
pub mod arp_addr;
pub mod config;
pub mod debug;
pub mod ext_config;
pub mod gpio_status;
pub mod hash_table;
pub mod hw_feat;
pub mod int;
pub mod l3l4;
pub mod lpi;
pub mod mdio;
pub mod mtl;
pub mod packet_filter;
pub mod phyif_ctrl_status;
pub mod rx_flow_ctrl;
pub mod rx_queue_ctrl;
pub mod tic_counter_us;
pub mod timestamp;
pub mod tx_queue_flow_ctrl;
pub mod tx_queue_priority;
pub mod vlan;
pub mod vlan_hash_table;
pub mod vlan_tag;
pub mod vlan_tag_data;

/// Creates Synopsys DesignWare Gigabit Ethernet MAC v4.xx register definitions.
///
/// Based on the register definitions from the Linux driver: <https://elixir.bootlin.com/linux/latest/source/drivers/net/ethernet/stmicro/stmmac/dwmac4.h>
///
/// Original Author: Alexandre Torque <alexandre.torque@st.com>
pub fn create() -> Result<Vec<svd::RegisterCluster>> {
    Ok([
        config::create()?,
        ext_config::create()?,
        packet_filter::create()?,
        hash_table::create()?,
        vlan_tag::create()?,
        vlan_tag_data::create()?,
        vlan_hash_table::create()?,
        vlan::create()?,
        tx_queue_flow_ctrl::create()?,
        rx_flow_ctrl::create()?,
        tx_queue_priority::create()?,
        rx_queue_ctrl::create()?,
        int::create()?,
        pmt::create(0xc0)?,
        lpi::create()?,
        tic_counter_us::create()?,
        pcs::create(0xe0)?,
        phyif_ctrl_status::create()?,
        debug::create()?,
        hw_feat::create()?,
        mdio::create()?,
        gpio_status::create()?,
        arp_addr::create()?,
        addr::create()?,
        l3l4::create()?,
        timestamp::create()?,
        mtl::create()?,
    ]
    .into())
}
