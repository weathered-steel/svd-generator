# svd-generator

Conversion tool for parsing device information from flattened device tree files, and generating a CMSIS-SVD description.

## Building

Build using a standard Rust workflow:

```bash
$ cargo build [--release]
```

## Running

`svd-generator` produces a CLI binary, you can run with:

```bash
# Using `cargo-run`
$ cargo run -- -f <flatted-device-tree.dtb> [-o <output.svd>]

# Running directly using the binary file
$ svd-generator -f <flattened-device-tree.dtb> [-o <output.svd>]
```

The default output file is `out.svd`.

For help information:

```bash
$ svd-generator -h
```

## Adding known peripherals

Only known peripherals will have SVD entries added.

This is because register definitions are not included in device tree files.

Register definitions are typically parsed from vendor provided manuals, firmware/kernel source code, and/or hardware definitions (VHDL, Verilog, etc.).

Currenty, there are a limited number of supported peripherals. They can be used as examples for adding new peripherals.

Peripheral definitions go in the [svd/peripheral](src/svd/peripheral) directory as a module.

For example, the Synsopsys DesignWare APB I2C peripheral definition: [dw_apb_i2c](src/svd/peripheral/dw_apb_i2c.rs).

New peripherals should follow to convential Rust module naming pattern:

- peripheral module: `src/svd/peripheral/<peripheral-model-name>.rs`
- peripheral sub-modules: `src/svd/peripheral/<peripheral-model-name>/`
- peripheral registers submodule: `src/svd/peripheral/<peripheral-model-name>/registers.rs`
- peripheral register definitions: `src/svd/peripheral/<peripheral-model-name>/registers/{<reg0-name>.rs, <reg1-name>.rs}`

Defining peripherals in this way should allow for a large degree of reusability among different SoCs/boards.

New entries should also be added to the `Compatible` enum: [tree/compatible](src/tree/compatible.rs).

## WIP

Currently, only binary-encoded flattened device tree (.dtb) is supported.

Work is ongoing to support source-encoded device tree (.dts).
