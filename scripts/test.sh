#!/usr/bin/env bash

in_dtb="dt/jh7110.dtb"
out_svd="out.svd"
out_dir="/tmp/svd-gen-out"

# Run svd-generator to convert DTB to SVD
cargo run -- -f $in_dtb -o $out_svd

# Make temporary output directories
mkdir -p ${out_dir}/src

# FIXME: add more architectures as more SoCs are supported
if [[ ! -f ${out_dir}/Cargo.toml ]]; then
	cp templates/riscv/Cargo.toml ${out_dir}/
fi

# Generate Rust project from the output SVD
svd2rust --target=riscv -i ${out_svd} -o ${out_dir}
form -i ${out_dir}/lib.rs -o ${out_dir}/src

# Run Cargo project setup and formatting on the new project

pushd ${out_dir}
rm lib.rs
cargo fmt
cargo build
cargo test --all --all-features
popd
